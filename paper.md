---
title: "Predihood: an open-source tool for predicting neighbourhoods' information"
tags:
  - Python
  - MongoDB
  - data management
  - neighbourhood
  - prediction
  - machine learning
authors:
  - name: Nelly Barret
    orcid: 0000-0002-3469-4149
    affiliation: 1
  - name: Fabien Duchateau
    orcid: 0000-0001-6803-917X
    affiliation: 1
  - name: Franck Favetta
    orcid: 0000-0003-2039-3481
    affiliation: 1
affiliations:
 - name: LIRIS UMR5205, Université Claude Bernard Lyon 1, Lyon, France
   index: 1
date: 16 September 2020
bibliography: paper.bib
---
# Introduction

Neighbourhoods are a very common concept in studies from diverse domains such as health, social sciences, or biology. For instance, Japanese researchers investigated the relationships between social factors and health by taking into account not only behavioural risks, but also housing and neighbourhood environments [@takada2014japanese]. In a British study, authors describe how living areas have an impact on physical activities, from which they determine a walkability index at the neighbourhood level for improving future urban planning [@frank2010development]. Another survey describes the luxury effect, i.e., the impact of wealthy neighbourhoods on the surrounding biodiversity [@leong2018biodiversity]. 
Several works focus on qualifying neighbourhoods using social networks. For instance, the Livehoods project defines and computes dynamics of neighbourhoods [@cranshaw2012livehoods] while the Hoodsquare project detects similar areas based on Foursquare check-ins [@zhang2013hoodsquare]. Crowd-based systems are interesting but may sometimes be biased, and they require technical skills to extract relevant data. [DataFrance](https://datafrance.info/) is an interface that integrates data from several sources, such as indicators provided by the National Institute of Statistics ([INSEE](https://insee.fr/en/)), geographical information from the National Geographic Institute ([IGN](http://www.ign.fr/institut/activites/geoservices-ign)) and surveys from newspapers for prices (L'Express). DataFrance enables the visualization of hundreds of indicators, but makes it difficult to judge on the main characteristics of a neighbourhood and is limited to France.
Despite all these works, there is no simple tool to visualize and predict insights about neighbourhoods.

The Predihood tool fills this gap by defining neighbourhoods, their characteristics and variables to be predicted. It includes a cartographic interface for searching and displaying information about neighbourhoods. Domain experts can provide a few examples of annotated neighbourhoods, and Predihood provides a configuration interface for using popular machine-learning algorithms in order to predict variables for the remaining neighbourhoods. The Predihood tool has been currently used to measure the impact of the neighbourhood's environment when people moves in another city [@data2020]. But it can be extended to other application domains: measuring the pollution degree in neighbourhoods, determining whether a neighbourhood is suitable as stopover for migratory birds, predicting evolution of neighbourhoods based on historical data, etc. In the rest of this paper, we describe the main features of Predihood and we explain how to extend them.

# Methodology

Predihood provides the following functionalities:

- adding new neighbourhoods and indicators to describe them;
- predicting variables of a neighbourhood by configuring and using predefined algorithms;
- adding new predictive algorithms.

In order to facilitate understanding, we describe and illustrate these functionalities based on a simple example, that aims at evaluating which neighbourhood is preferable for migratory birds to make a temporary stop. We only include three indicators per neighbourhood: the percent of greens, the percent of buildings and the degree of human pressure. A single variable `migration zone` qualifies a neighbourhood from _favorable_ to _unfavorable_.

## Adding new datasets

As Predihood is a general purpose application, it enables contributors to add their own datasets. The key concept of a dataset is the neighbourhood, which is represented as a [GeoJSON object](https://geojson.org/) including:

- a geometry (multi-polygons), which describes the shape of the neighbourhood. This crucial data is not only useful for cartographic visualization but also enable automatic calculations such as area;
- properties, which are divided into two categories. Descriptive information (e.g., name, city postcode) mainly aim at improving display while a set of quantitative indicators is used for predicting the values of the variable.

Besides, some neighbourhoods have to be manually annotated, a task typically performed by domain experts. To add a new dataset, it is necessary to store them as GeoJSON and make them accessible by Predihood, for instance in a document-oriented database. 

To build a dataset for the _bird migration_ example, it is necessary to collect and eventually to integrate data source(s) about neighbourhoods of the studied geographic area. Values for the three indicators should be provided, and a value to the `migration zone` variable should be assigned to a few neighbourhoods.

## Predicting

Machine learning algorithms require data preparation by grouping useful properties and variables. We illustrate this step on the _bird migration_ dataset, as shown by Figure 1. Predihood produces a table composed of the identifier and the name of the neighbourhood (grey columns), its indicators (yellow columns) that could be normalized by factors such as density of population (green columns) and optionally the assessment of researchers for the `migration zone` variable (blue column). The objective of Predihood is to fill automatically question marks for neighbourhoods that are not yet assessed.

![A subset of the computed dataset.](doc/predihood-indicators-bird.png)

To perform prediction, a selection process first selects subsets of relevant indicators, since too many indicators may degrade performance. The tool automatically reduces the number of indicators, e.g., by removing indicators with a unique value or those highly correlated using Spearman coefficient. Then, Predihood generates 7 lists of indicators, containing from 10 to 100 indicators. The current version of Predihood includes 8 predictive algorithms from [scikit-learn](https://scikit-learn.org/) (e.g., Random Forest, KNeighbours). Depending on the algorithm, small or large lists of indicators may be more effective. Predihood provides a cartographic web interface based on [Leaflet](https://leafletjs.com/) and [Open Street Map](https://www.openstreetmap.org/), as shown in Figure 2. We have searched for query _lyon_ (left panel) and all neighbourhoods containing this query in their name or their city names are shown in blue on the map. We have selected the neighbourhood [_Le parc_](https://en.wikipedia.org/wiki/Parc_de_la_Tête_d%27or) and we run the Random Forest classifier: migration is considered _very favorable_ in this area (for the seven lists of indicators). Indeed, this park seems relevant for bird migration as it has nice green areas for birds and it is a healthy environment for them.

![Screenshot of the cartographic interface of Predihood](doc/predihood-predictions-bird.png)

## Adding new algorithms

Because prediction is a complex task, testing specific algorithms tuned with different parameters and comparing their results may help increase the overall quality. In order to facilitate this task, Predihood proposes a generic and easy-to-use programming structure for machine learning algorithms, based on [Scikit-learn](https://scikit-learn.org/stable/) algorithms. Thus, experts can implement hand-made algorithms and run experiments in Predihood. Adding a new algorithm only requires 4 steps:

1. Create a new class that represents your algorithm, e.g. `MyOwnClassifier`, which inherits from `Classifier`;
2. Implement the core of your algorithm by coding the `fit()` and `predict()` functions. The `fit` function aims at fitting your classifier on assessed neighbourhoods while the `predict` function aims at predicting variable(s) for a given neighbourhood;
3. Add the `get_params()` function, which returns a dictionary of parameters along with their value, in order to be compatible with the Scikit-learn framework;
4. Comment your classifier with the [Numpy style](https://numpydoc.readthedocs.io/en/latest/format.html#docstring-standard) so that Predihood automatically extracts its parameters and enables their tuning in the dedicated interface.

Below is a very simple example to illustrate the aforementioned steps. Note that new algorithms are directly loaded into Predihood.

```python
# file ./algorithms/MyOwnClassifier.py
from predihood.classes.Classifier import Classifier


class MyOwnClassifier(Classifier):
  """Description of the classifier.
  Parameters
  ------------
  a : float, default=0.01
    Description of a.
  b : int, default=10
    Description of b.
  """

  def __init__(self, a=0.01, b=10):
    self.a = a
    self.b = b

  def fit(self, X, y):
    # do stuff here

  def predict(self, df):
    # do stuff here

  def get_params(self, deep=True):
    # suppose this estimator has parameters "a" and "b"
    return {"a": self.a, "b": self.b}
```

To facilitate experiments, Predihood provides an interface for easily tuning and testing algorithms on a dataset, as shown in Figure 3. The left panel stands for the selection of an algorithm and the tuning of its parameters and hyper parameters, such as training and test sizes. Note that two options enable to remove outliers and under-represented neighbourhoods (for a given variable) without directly modifying the dataset. On the right, the table illustrates the accuracies obtained for each list of indicators (generated during the selection process) and each variable. Results can be exported in XLS with the blue download icon. Here, we notice that the new algorithm _MyOwnClassifier_ has been chosen, and its parameters (_a_ and _b_) can be configured. We have performed 2 runs, the former with the Random Forest classifier and the latter with MyOwnClassifier. Best predictive results are achieved with all indicators (green cells).

![Screenshot of algorithmic interface of Predihood](doc/predihood-accuracies-bird.png)

# Mentions of Predihood

Our Predihood tool has been presented during the DATA conference [@data2020]. The main research challenge is to evaluate whether people choose a similar neighbourhood environment when moving out. The tool is bundled with data from France using the [mongiris](https://gitlab.liris.cnrs.fr/fduchate/mongiris) project (in which unit divisions named [IRIS](https://www.insee.fr/en/metadonnees/definition/c1523/) stand for neighbourhoods), and the dataset contains about 50,000 neighbourhoods with 640 indicators (about population, shops, buildings, etc.). Six environment variables have been defined (_building type_, _building usage_, _landscape_, _social class_, _morphological position_ and _geographical position_), and 270 neighbourhoods were annotated by social science researchers (one to two hours per neighbourhood to investigate building and streets pictures, parked cars, facilities and green areas from services such as Google Street View).
Prediction results achieved by Predihood using 6 algorithms from Scikit-learn range from 30% to 65% depending on the environment variable, and designing new algorithms could help improving these results.

The project is available here: [https://gitlab.com/fduchate/predihood](https://gitlab.com/fduchate/predihood).

# Acknowledgements

This work has been partially funded by LABEX IMU (ANR-10-LABX-0088) from Université de Lyon, in the context of the program "Investissements d'Avenir" (ANR-11-IDEX-0007) from the French Research Agency (ANR).

# References
