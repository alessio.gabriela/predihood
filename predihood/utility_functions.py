"""
Global methods for manipulating neighbourhoods, lists of indicators, introspection features, evaluation, configuration loading, etc.
"""

# eternal hack for python imports (here to access parent directory)
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

import ast
import inspect
import json
import logging
import numpy as np
import pandas as pd
import re
import requests
import stringdist

from area import area
from predihood import model
from predihood.classifiers_list import AVAILABLE_CLASSIFIERS
import predihood.config  # import TRAIN_SIZE, TEST_SIZE, ENVIRONMENT_VARIABLES, TRANSLATION, OLD_PREFIX, NEW_PREFIX, FILE_MANUAL_ASSESSMENT, CURRENT_CONFIG_DICT, PREFERRED_LANGUAGE

log = logging.getLogger(__name__)


# 1. Neighbourhood (IRIS) function
def address_to_code(address):
    """
    Get IRIS code from address.

    Args:
        address: a string containing the address of the iris

    Returns:
        a string containing the code of the searched address
    """
    response = requests.get("https://pyris.datajazz.io/api/search/", params=[("q", address)])
    json_response = response.json()
    return str(json_response["complete_code"]) if "complete_code" in json_response else None


def address_to_city(address):
    """
    Get city from address.

    Args:
        address: a string containing the address of the iris

    Returns:
        a string containing the city of the searched address
    """
    response = requests.get("https://pyris.datajazz.io/api/search/", params=[("q", address)])
    json_response = response.json()
    return str(json_response["city"]) if "city" in json_response else None


def append_indicator(raw_indicator, iris, lst, append_col, indicators):
    """
    Append the value of an INSEE indicator to a list corresponding to the data of the assessed IRIS (departure or arrival).

    Args:
        raw_indicator: a string containing the name of the indicator, e.g. P14_POP
        iris: the iris object that contains the indicator
        lst: the list where to append the indicator's value
        append_col: True to append the indicator to the cols variable
        indicators: the list of the columns of the dataset, i.e. the indicators

    Returns:
        the list containing indicator' values and the other one containing indicators names.
    """
    if iris is None: raise Exception("This neighbourhood does not have attributes.")
    if raw_indicator in iris["properties"]["raw_indicators"]:
        val_indicator = iris["properties"]["raw_indicators"][raw_indicator]
        # append NaN if indicator is null, else append the value
        lst.append(np.nan) if (val_indicator is None or not val_indicator) else lst.append(val_indicator)
    else:
        lst.append(np.nan)
    if append_col: indicators.append(raw_indicator)
    return lst, indicators


def append_target(row, target, lst):
    """
    Append the target to the current list (departure or arrival).

    Args:
        row: a list corresponding to the current row of dataset
        target: a string containing the target to append, i.e. the value of the EV
        lst: the list where the target is append

    Returns:
        the list with the appended target
    """
    if target == predihood.config.OLD_PREFIX + "geographical_position" or target == predihood.config.NEW_PREFIX + "geographical_position":
        # get only geo position without the city, e.g. South-East Lyon gives South-East
        if str(row[target]) == "nan":
            city = np.nan
        else:
            city = row[target].split(" ")[0]
        # city_name = ''.join(split_city[1:len(split_city)])
        value = predihood.config.TRANSLATION[city] if city in predihood.config.TRANSLATION else np.nan
        lst.append(value)
    else:
        value = row[target] if row[target] else np.nan
        lst.append(value)
    return lst


def indicator_full_to_short_label(full_label):
    """
    Convert the full label of an indicator to its short label.

    Args:
        full_label: a string containing the full label of the indicator, e.g. Population 0-2 y.o. in 2014

    Returns:
        the short label of the given indicator, e.g. P14_POP0002
    """
    indicators = model.get_indicators_dict()
    key_list = list(indicators.keys())
    val_list = list(indicators.values())
    if full_label not in indicators.values():
        return key_list[val_list.index(full_label + " ")]
    else:
        return key_list[val_list.index(full_label)]


def indicator_short_to_full_label(short_label):
    """
    Convert the short label of an indicator to its full label.

    Args:
        short_label: a string containing the short label of the indicator, e.g. P14_POP0002

    Returns:
        the full label of the indicator, e.g. Population 0-2 y.o. in 2014
    """
    indicators = model.get_indicators_dict()
    return indicators[short_label]


def apply_hierarchy(selected_indicators, hierarchy):
    """
    Apply hierarchy on the given indicators in order to go up children indicators in their parents.

    Args:
        selected_indicators: a list of indicators selected by the feature importance process.
        hierarchy: the hierarchy of the indicators, i.e. for each indicator, there are its level in the hierarchy and its first ancestor.

    Returns:
        the new selected indicators list, with some children which have go up in their parents.
    """
    list_indicators_FI = [selected_indicators[j][0] for j in range(len(selected_indicators))]
    indexes_to_remove = []
    for i in range(len(selected_indicators)):
        index_row = hierarchy.index[hierarchy["INDICATOR"] == selected_indicators[i][0]].tolist()
        if len(index_row) == 0:
            continue  # pass this indicator because it does not exist in the hierarchy
        else:
            index_row = index_row[0]
        level = hierarchy.iloc[index_row, 2]
        ancestor = hierarchy.iloc[index_row, 3]
        if level > 1:
            if ancestor in list_indicators_FI:
                index_ancestor = list_indicators_FI.index(ancestor) if ancestor in list_indicators_FI else None
                while index_ancestor in indexes_to_remove:
                    ancestor2 = hierarchy.iloc[hierarchy.index[hierarchy["INDICATOR"] == ancestor].tolist()[0], 3]  # name of ancestor
                    ancestor = ancestor2
                    index_ancestor = list_indicators_FI.index(ancestor) if ancestor in list_indicators_FI else None
                    if hierarchy.iloc[hierarchy.index[hierarchy["INDICATOR"] == ancestor].tolist()[0], 2] == 1:
                        break
                if index_ancestor not in indexes_to_remove:
                    selected_indicators[index_ancestor][1] += selected_indicators[i][1]
                    indexes_to_remove.append(i)
    for index in sorted(indexes_to_remove,reverse=True):  # remove in reverse order to do not throw off subsequent indexes
        del selected_indicators[index]
    return selected_indicators


def get_classifier(name):
    """
    Get an instance of a classifier with its name.

    Args:
        name: a string containing the name of the desired classifier

    Returns:
        an instance of a classifier
    """
    classifier_object = AVAILABLE_CLASSIFIERS[name]
    return classifier_object()


def set_classifier(classifier, parameters):
    """
    Tune the classifier with the given parameters.

    Args:
        classifier: an instance of the classifier to tune
        parameters: a dictionary containing the tuning parameters

    Returns:
        an instance of the tuned classifier
    """
    keys_clf = list(classifier.get_params().keys())
    # remove None parameters and parameters that don't exist in sklearn (e.g. train and test size)
    parameters = {key: value for key, value in parameters.items() if value != "" and key in keys_clf}
    classifier.set_params(**parameters)
    return classifier


def signature_of_algorithm(chosen_algorithm):
    """
    Get the signature of an algorithm, i.e. its parameters, the default values and the type of each parameter. The documentation of the algorithm must be in NumPy style.

    Args:
        chosen_algorithm: a string containing the name of the algorithm, e.g. RandomForestClassifier

    Returns:
        the signature of the given algorithm, i.e. a dictionary containing for each parameter:
        - a list of the accepted types
        - the default value
        - a description of the parameter (e.g. "The train_size parameter aims at tuning the size of the sample during the learning step.")
    """
    if chosen_algorithm == "Algorithm": return json.dumps({}), "no_selection"  # special case for no selection

    # getting parameters and default values
    parameters = {}
    model = get_classifier(chosen_algorithm)
    if sys.version_info.major < 3 or (sys.version_info.major == 3 and sys.version_info.minor < 8):
        params = inspect.getfullargspec(model.__init__).args[
                 1:]  # get parameter' names -- [1:] to remove self parameter
        defaults = inspect.getfullargspec(model.__init__).defaults  # get default values
    else:
        # getfullargspec is deprecated since Python 3.8, need to use inspect.signature instead
        sig = inspect.signature(model.__init__)
        params, defaults = [], []
        for param in sig.parameters.values():
            params.append(param.name)
            defaults.append(param.default)
    if params is None or defaults is None: return json.dumps({}), "no_parameters"
    assert len(params) == len(defaults)

    # getting the definition of parameters
    try:
        doc = model.__doc__
        param_section = "Parameters"
        dashes = "-" * len(param_section)  # -------
        number_spaces = doc.find(dashes) - (doc.find(param_section) + len(param_section))
        attribute_section = "Attributes\n"
        # sub_doc is the param section of the docs (i.e. without attributes and some text)
        sub_doc = doc[doc.find(param_section) + len(param_section) + number_spaces + len(dashes) + len("\n"):doc.find(attribute_section)]
    except:
        return json.dumps({}), "no_documentation"

    for i in range(len(params)):
        param_name = str(params[i]) + " : "
        index_param = sub_doc.find(param_name)
        if index_param == -1: continue  # the param is not present in the doc
        index_next_newline = sub_doc[index_param:].find("\n")  # find returns the first occurrence
        parameter_string = sub_doc[index_param:index_param + index_next_newline]
        doc_param = sub_doc[index_param + index_next_newline:]
        index_end_sentence = re.search("(\.\s)", doc_param).start()  # search for the first sentence
        first_sentence = doc_param[:index_end_sentence + 1]
        # format first sentence to have a prettier display.
        first_sentence = first_sentence.replace("\n", " ")
        while "  " in first_sentence:
            first_sentence = first_sentence.replace("  ", " ")
        types_and_default = parameter_string[len(param_name):]
        if "{" in types_and_default and "}" in types_and_default:  # for cases like {"auto", "kd_tree", "brute"}, optional
            types_and_default = types_and_default.replace("{", '')
            types_and_default = types_and_default.replace("}", '')
        if " or " in types_and_default: types_and_default = types_and_default.replace(" or ", ", ")
        types_defaults_split = types_and_default.split(", ")
        types = []
        default = -1
        variants = ["optional (default=", "optional (default = ", "optional", "(default=", "(default = ", "default ",
                    "default: ", "default="]  # DO NOT CHANGE THE ORDER OF ITEMS
        for item in types_defaults_split:
            if not any(value in item for value in variants):
                if item.startswith("length"):
                    pass  # exceptions
                else:
                    types.append(item)  # item is a type
            else:
                for value in variants:
                    if value in item:
                        if value.startswith("optional ("):
                            default = item.split(value)[1][:-1]
                        elif value.startswith("(default"):
                            default = item.split(value)[1][:-1]
                        elif value.startswith("default"):
                            default = item.split(value)[1]
                        elif value == "optional":
                            default = "None"
                        break  # do not iterate over other values
        if default != -1 and default != "None":
            type_of_default = str(type(ast.literal_eval(str(default))).__name__)
        else:
            type_of_default = "str"
        types[:] = ["int" if x == "integer" else x for x in types]  # replace "integer" by "int"
        types[:] = ["bool" if x == "boolean" else x for x in types]  # replace "boolean" by "bool"
        types[:] = ["str" if x == "string" else x for x in types]  # replace "str" by "string"
        if len(types) == 0: types.append(type_of_default)  # fill missing types
        types[:] = [x for x in types if "None" not in x and "NoneType" not in x]  # remove None type
        parameters[param_name[:-3]] = {"types": types, "default": default, "description": first_sentence}  # -3 to remove " : "
    return parameters, "OK"


def check_train_test_percentages(train_size, test_size):
    """
    Check train and test size and update with defaults or divided by 100 if needed.

    Args:
        train_size: an integer or a float corresponding to the value for train size (should be between 0 and 1)
        test_size: an integer or a float corresponding to the value for test size (should be between 0 and 1)

    Returns:
        the train and test sizes
    """
    if 0 < train_size < 1 and 0 < test_size < 1 and train_size + test_size == 1:
        message = "OK"  # default case - everything is correct
    elif 0 < train_size < 1 and 0 < test_size < 1:
        message = "KO"  # train_size or test_size is not correct so we set default parameters
        train_size = predihood.config.TRAIN_SIZE  # 0.8
        test_size = predihood.config.TEST_SIZE  # 0.2

    if 1 <= train_size < 100 and 1 <= test_size < 100 and train_size + test_size == 100:
        message = "OK"  # default case - everything is correct
        train_size = train_size / 100
        test_size = test_size / 100
    elif 1 <= train_size < 100 and 1 <= test_size < 100:
        message = "KO"  # train_size or test_size is not correct so we set default parameters
        train_size = predihood.config.TRAIN_SIZE  # 0.8
        test_size = predihood.config.TEST_SIZE  # 0.2
    return train_size, test_size, message


def add_assessment_to_file(code_iris, values):
    """
    Add an assessed IRIS to the CSV file.

    Args:
        code_iris: a string corresponding to the code of the IRIS (9 digits)
        values: a list of values for the 6 EV that represent the environment of the assessed IRIS

    Returns:
        the string "okay" if the assessed IRIS has been added to the CSV file
    """
    df = pd.read_csv(predihood.config.FILE_MANUAL_ASSESSMENT)
    codes = df["CODE"].tolist()
    codes_lst = [str(elem) for elem in codes]
    if code_iris in codes_lst:
        return "iris already assessed"
    else:
        iris_data = model.get_neighbourhood_from_code(code_iris)
        iris_coords = model.get_coords_from_code(code_iris)
        area_iris = area(iris_coords) / 1000000 if iris_coords is not None else None
        density_iris = iris_data["properties"]["raw_indicators"]["P14_POP"] / area_iris if area_iris is not None and area_iris > 0 else None

        iris = []
        cols = ["CODE", "AREA", "DENSITY"]
        # adding code, area and density
        iris.append(code_iris)
        iris.append(area_iris)
        iris.append(density_iris)

        # adding insee indicators
        indicators = model.get_indicators_list()
        indicators_to_remove = ["IRIS", "REG", "DEP", "UU2010", "COM", "LIBCOM", "TRIRIS", "GRD_QUART", "LIBIRIS", "TYP_IRIS", "MODIF_IRIS", "LAB_IRIS", "LIB_IRIS", "LIB_COM", "CODGEO", "LIBGEO"]
        for indicator in indicators_to_remove:
            if indicator in indicators:
                indicators.remove(indicator)

        for raw_indicator in indicators:
            iris, cols = append_indicator(raw_indicator, iris_data, iris, True, cols)

        iris.extend(values)  # adding assessed values
        cols.extend(predihood.config.ENVIRONMENT_VARIABLES)
        df = pd.DataFrame([iris])
        df.to_csv(predihood.config.FILE_MANUAL_ASSESSMENT, mode="a", index=False, header=False)  # DO NOT ERASE HEADER IN THE CSV FILE
        return "okay"


# 2. list functions
def intersection(lst1, lst2):
    """
    Intersect two lists.

    Args:
        lst1: a list corresponding to the first list to be intersected
        lst2: a list corresponding to the second list to be intersected

    Returns:
        a list corresponding to the result of the intersection of the two given lists.
    """
    return list(set(lst1) & set(lst2))


def union(lst1, lst2):
    """
    Unify two lists without repetitions.

    Args:
        lst1: a list corresponding to the first list to append
        lst2: a list corresponding to the second list to append

    Returns:
        a list corresponding to the union of the two lists
    """
    return list(set(lst1) | set(lst2))


def similarity(value, lst):
    """
    Check if a value is enough similar to the data.

    Args:
        value: the value on which the similarity is computed
        lst: the list containing other values to check similarity

    Returns:
        the index of the similar value, -1 if no value is enough similar, -2 if the value is already added
    """
    for i in range(len(lst)):
        if value in lst[i]: return -2  # this value is already append
        for elem in lst[i]:
            if not isinstance(elem, float):
                dissimilarity = stringdist.levenshtein(str(elem), str(value))  # compute Levenshtein similarity
                if dissimilarity == 1: return i  # if the given value has only one difference with the current value, store it as a similarity
    return -1


def get_most_frequent(lst):
    """
    Get the most frequent item in a list. If many elements are frequent, it returns the first one.

    Args:
        lst: the list to find the most frequent element

    Returns:
        a dictionary containing the most frequent of the given list and its count
    """
    most_frequent_element = max(set(lst), key=lst.count)
    dictionary = {"most_frequent": most_frequent_element, "count_frequent": lst.count(most_frequent_element)}
    return dictionary


# 3. plot functions
def auto_label(rectangles, axes):
    """
    Adds a text label above each bar in rectangles, displaying its value.

    Args:
        rectangles: the bars of the plot.
        axes: the axes of the plot.
    """
    for rect in rectangles:
        height = rect.get_height()
        axes.annotate("{}".format(height), xy=(rect.get_x() + rect.get_width() / 2, height), xytext=(0, 3), textcoords="offset points", ha="center", va="bottom")


# 4. other utility functions
def get_env_name_translation(env_name, language):
    return predihood.config.CURRENT_CONFIG_DICT["VARIABLES_VALUES"][language][env_name]["label"]


def get_env_value_translation(env_value):
    if env_value in predihood.config.TRANSLATION:
        return predihood.config.TRANSLATION[env_value]
    else:
        return env_value


def create_config_dictionaries(config_dict):
    predihood.config.CURRENT_CONFIG_DICT = config_dict
    variable_values = config_dict["VARIABLES_VALUES"]
    target_values = {}
    for lang in variable_values:  # iterate over languages
        # if lang != "en":
        target_values["en_" + lang] = {}
        for var_name in variable_values[lang]:  # iterate over variables to predict
            target_values["en_" + lang][var_name] = {}
            for i, value in enumerate(variable_values[lang][var_name]["values"]):
                target_values["en_" + lang][var_name][variable_values["en"][var_name]["values"][i]] = value
    predihood.config.TARGET_VALUES = target_values

    predihood.config.ENVIRONMENT_VALUES = target_values["en_"+predihood.config.PREFERRED_LANGUAGE]

    # names of EV, i.e. ['building_type', 'building_usage', 'landscape', 'morphological_position', 'geographical_position', 'social_class']
    local_environment_variables = [var_name for var_name in variable_values[predihood.config.PREFERRED_LANGUAGE].keys()]
    predihood.config.ENVIRONMENT_VARIABLES = local_environment_variables

    # translation of each value from French to English, ie. without the level with EV' names
    translation = {}
    for ev in predihood.config.ENVIRONMENT_VALUES:
        for key in predihood.config.ENVIRONMENT_VALUES[ev]:
            translation[key] = predihood.config.ENVIRONMENT_VALUES[ev][key]
    predihood.config.TRANSLATION = translation



def get_translation_for_language(word, language):
    """
    Get
    Args:
        word: a string containing the word to be translated into the given language
        language: a string containing the abbreviation of the language ("fr" for French, "en" for English, ...)

    Returns:

    """
    exit(0)
    # if CURRENT_CONFIG_DICT[language]:
    #     if word in

# def get_text(key, language):
#     """
#     Get the translation of the phrase (identified by the key) stored in translations (used to translate HTML pages).
#
#     Args:
#         key: a string which is the key under which the phrase is stored.
#         language: a string which is the language under which the translation will be done.
#
#     Returns:
#         a string containing the phrase in the given language, else the phrase in english.
#     """
#     if key not in translations[language]:
#         return translations["english"][key]
#     return translations[language][key]


# 5. Dataset config file loading
def load_dataset_config(json_file_path):
    """
    Load data from a configuration JSON file, which contains information about a new dataset (e.g., variable name,
    database name, collection name).
    Check examples of configuration files in directory configs/.

    Args:
        json_file_path: the path to dataset config JSON file.
    """
    with open(json_file_path) as data_file:
        data = json.load(data_file)
        data_file.close()
    if data:
        log.info("The dataset config file has been successfully loaded.")
        return data
    else:
        log.warning(f"A problem occurred while parsing JSON dataset config file {json_file_path} !")
    return None

