/**
 * Send an AJAX request to get predictions for the given IRIS.
 * @param iris_code a string containing the code of the IRIS to predict.
 * @param algorithm_name a string containing the name of the algorithm used to predict environment.
 * @param popup the minimal popup (o be updated with writePopup).
 * @param layer th layer (Leaflet) on which the popup will be added.
 * @param e the event of the click
 * @returns {} a dictionary containing results of predictions, i.e. a value and a score for each EV.
 */
function predict(iris_code, algorithm_name, popup, layer, e) {
	document.body.style.cursor = 'wait'; // change body cursor
	// $(".leaflet-grab").css("cursor", "wait"); // change leaflet cursor
	// $(".leaflet-container").css("cursor", "wait"); // change leaflet cursor
	$(".leaflet-clickable, .leaflet-container, .leaflet-interactive").css("cursor", "wait"); // change leaflet cursor
	console.log(iris_code + " " + algorithm_name)

	if(typeof iris_code === 'string' || iris_code instanceof String) {
		// predict one IRIS
		console.log("predict one neighbourhood/IRIS");
		// updatePopup(layer, algorithm_name, iris_code, divInformation, undefined, e); // refresh popup (no prediction yet)
		$.ajax({
			type: "GET",
			url: "/predict-neighbourhood",
			data: {
				'iris_code': iris_code,
				'algorithm_name': algorithm_name
			},
			contentType: 'application/json;charset=UTF-8',
			success: function(result) {
				console.log(result['predictions']);
				let predictions = result['predictions'];
				if(result['message'] === "KO") { alert("Your train_size or test_size parameter was set badly. The algorithm has been run with default parameters which are 80% for train_size and 20% for test_size."); }

				// write popup with the result of the prediction
				console.log(predictions)
				writePopup(predictions, iris_code, algorithm_name, popup, layer, e);
				$(".downloadPopup").addClass("colorfulIcon");
				document.body.style.cursor = 'default';
				$(".leaflet-clickable, .leaflet-container, .leaflet-interactive").css("cursor", "grab"); // change leaflet cursor
				alert("The prediction for neighbourhood " + iris_code + " has finished. You can download the result by clicking on the blue download icon.");
			},
			error: function(result, textStatus, errorThrown) {
				console.log(errorThrown);
				alert("There is an issue to predict environment of this neighbourhood. You can check the console to have more details. " + result + textStatus + errorThrown);
				document.body.style.cursor = 'default';
				$(".leaflet-clickable, .leaflet-container, .leaflet-interactive").css("cursor", "grab"); // change leaflet cursor
			}
		});
	} else { // if(iris_code.size > 0) {
		// predict many IRIS
		var all_predictions = {};
		var iris_code_for_python = {}
		var count = 0;

		// convert the array (["123456789", "0987654321", ...] to a dictionary ({"0": "123456789", "1": "987654321"}).
		for(var code of iris_code) {
			iris_code_for_python[count.toString()] = code;
			count += 1;
		}
		console.log("The following neighbourhoods will be predicted: " + iris_code);

		$.ajax({
			type: "GET",
			url: "/predict-neighbourhoods",
			data: {
				'list_codes': JSON.stringify(iris_code_for_python),
				'algorithm_name': algorithm_name
			},
			contentType: 'application/json;charset=UTF-8',
			success: function(result) {
				console.log(result);
				console.log(result['predictions']);
				all_predictions = result['predictions'];
				if(result['message'] === "KO") { alert("Your train_size or test_size parameter was set badly. The algorithm has been run with default parameters which are 80% for train_size and 20% for test_size."); }

				// write Excel file when predicting many IRIS
				var table_predictions = $("<table>").prop("id", "tablePredictions");
				var row = null;
				var filename = "predictions_for_"
				for(var code in all_predictions) {
					var iris_prediction = all_predictions[code];

					// 1. add each result to the table, 2. update the filename
					row = $("<tr>");
					row.append($("<td>").prop("colspan", "3").text(code));
					table_predictions.append(row);
					for(var key in iris_prediction) {
						var prediction = iris_prediction[key];
						// console.log(key + " - " + prediction["most_frequent"] + " " + prediction["count_frequent"]);
						row = $("<tr>");
						row.append($("<td>").text(key)); // add the name of the environment variable
						row.append($("<td>").text(prediction["most_frequent"])); // add the predicted value (i.e. the most frequent one according lists)
						row.append($("<td>").text("(" + prediction["count_frequent"] + " / 7)")); // add the score (e.g. 5/7)
						table_predictions.append(row);
					}

					filename += code + "_";
				}

				$("#downloadManyIris")
					.addClass("colorfulIcon")
					.on("click",function () {
						table_predictions.table2excel({
							type: 'xls',
							filename: filename + '.xls',
							preserveColors: true
						});
					});

				document.body.style.cursor = 'default';
				$(".leaflet-clickable, .leaflet-container, .leaflet-interactive").css("cursor", "grab"); // change leaflet cursor
				alert("The predictions for the selected neighbourhoods are finished. You can download the result by clicking on the blue download icon.")
			},
			error: function(result, textStatus, errorThrown) {
				console.log(errorThrown);
				alert("There is an issue to predict environment of this neighbourhood. You can check the console to have more details. " + result + textStatus + errorThrown);
				document.body.style.cursor = 'default';
				// $(".leaflet-grab").css("cursor", "grab"); // change leaflet cursor
				// $(".leaflet-container").css("cursor", "grab"); // change leaflet cursor
				$(".leaflet-clickable, .leaflet-container, .leaflet-interactive").css("cursor", "grab"); // change leaflet cursor
			}
		});
	}
}


