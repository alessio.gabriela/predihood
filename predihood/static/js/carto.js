/*
* Functions for Leaflet.js cartographic library (init Leaflet map, load a GeoJSON file as a layer, etc.)
*/

// parameters
const ZOOM_SNAP = 0.5;  // step value for which the zoom increases/decreases (0.5 for finer tuning, else 1.0)
const DEFAULT_ZOOM_LEVEL_MIN_FOR_DISPLAY = 12; // zoom level above which iris are displayed (default 12)
const NB_IRIS_MAX_FOR_DISPLAY = 250; // max number of iris iris to enable display (default 250) - not used, the costly
// operation is to extract iris (>500) from database, not the display or json parsing

// global variables
let map = null; // leaflet map
let baseLayers = null; // array of basic layers
let overlayLayers = null; // array of overlaying layers
let selected_iris = []; // to store multiple selection of iris/neighbourhoods
let osmLayer = null; // openstreetmap basic layer
let irisLayer = null; // layer of displayed IRIS
let previously_selected_algorithm = null;
let environment_variables = getEnvironmentVariables(); // get the json corresponding to the list of environment variables with the possible values
let preferred_language_carto = get_preferred_language(); // get the language chosen in index.html
addClassifiers("#selectManyIrisAlgo");
let selected_codes = []; // to store codes of iris/neighbourhoods that have been selected
let predictions = null;

/**
 * Initialize the map
 */
function initialize() {
    // creation of the map and general settings : option zoom snap degree, centering, zoom level
    map = L.map("map-id", {zoomSnap: ZOOM_SNAP}).setView([46.895143, 2.397461], 6);
    // creation of an OSM layer
    osmLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        "attribution": 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
        "maxZoom": 18,
        "detectRetina": false,
        "minZoom": 1,
        "noWrap": false,
        "subdomains": "abc"
    }).addTo(map);
    // legend for layers
    baseLayers = {'OpenStreetMap' : osmLayer};
    // add the zoom change event
    map.addEventListener("zoomend", zoomendEvent);
}


/**
 * Event for zoom changes : updates a label and if zoom enabled and above min zoom level, display iris
 */
function zoomendEvent() {
    zoomLevel = map.getZoom();
    document.getElementById("spanZoomLevel").innerHTML = zoomLevel;
    isZoomDisabled = $("#inputZoomLevel").prop("disabled");
    if(!isZoomDisabled) {  // display after zoomend is enabled
        let minZoomLevel = $("#inputZoomLevel").val();
        if(zoomLevel >= minZoomLevel) { // display iris on the selected zone
            bounds = map.getBounds();
            iris = getNeighbourhoodsForBounds(bounds);
        }
    }
}

/**
 * Method for deleting a layer (e.g., all iris in irisLayer)
 */
function removeLayer() {
    map.removeLayer(irisLayer);
    irisLayer = null;
    $("#zoneMessages").html("");
}


/**
 * Display popup with several information about the neighbourhood (descriptive information and environment variables) when clicking on it.
 * @param e the event corresponding to the click.
 */
function displayMinimalPopup(e) {
    let layer = e.target;
    let iris_code = layer.feature.properties.CODE_IRIS;

    // common part of the popup
    let divInformation = $("<div>");
    let download;
    let moreInfosLink = $("<a>");
    if(preferred_language_carto === "fr") {
        download = $("<i class='fas fa-download downloadPopup' style='margin-left: 1rem;' title='Exporter cette table comme un fichier Excel.'></i>");
        divInformation
            .append("CODE : " + iris_code).append($("<br>"))
            .append("NOM : " + layer.feature.properties.NOM_IRIS).append($("<br>"))
            .append("COMMUNE : " + layer.feature.properties.NOM_COM).append($("<br>"))
        moreInfosLink
            .prop("href", "details-neighbourhood.html?neighbourhood_code="+iris_code)
            .prop("target", "_blank")
            .text("Plus de détails")
            .append($("<br>"));
    } else {
        download = $("<i class='fas fa-download downloadPopup' style='margin-left: 1rem;' title='Export this table as an Excel file.'></i>");
        divInformation
            .append("CODE: " + iris_code).append($("<br>"))
            .append("NAME: " + layer.feature.properties.NOM_IRIS).append($("<br>"))
            .append("TOWNSHIP: " + layer.feature.properties.NOM_COM).append($("<br>"))
        moreInfosLink
            .prop("href", "details-neighbourhood.html?neighbourhood_code="+iris_code)
            .prop("target", "_blank")
            .text("More details")
            .append($("<br>"));
    }
    divInformation.append(moreInfosLink).append(download);

    let selectAlgorithm = $("<select>").prop("id", "selectAlgorithmTooltip").append($("<option>").prop("value", "undefined").text("---"))
    for(let algorithm of classifiers) { selectAlgorithm.append($("<option>").prop("value", algorithm).text(algorithm)); }

    let popup = divInformation[0].outerHTML + selectAlgorithm[0].outerHTML;
    layer.bindPopup(popup)
    layer.bringToFront();
    layer.openPopup();

    $("#selectAlgorithmTooltip").on("click", function() {
        let selected_algorithm = $("#selectAlgorithmTooltip option:selected").val();
        predict(iris_code, selected_algorithm, popup, layer, e)
    });
}

/**
 * Write the predictions in the popup (divPredictions) and the Excel file with the results (tablePredictions).
 * @param predictions construct the table of predictions (to be used to generate the XLS file)
 * @param code_iris the code of the predicted IRIS/neighbourhood
 * @param algorithm_name
 * @param old_popup
 * @param layer
 * @param e
 * @returns {string} the div element with predictions' content
 */
function writePopup(predictions, code_iris, algorithm_name, old_popup, layer, e) {
    if (predictions !== undefined) {
        let divPredictions = $("<div>").prop("id", "divPredictions");
        let tablePredictions = $("<table>").prop("id", "tablePredictions");
        tablePredictions.append("<th><td>Environment value</td><td>Prediction</td><td>Frequency</td></th>")
        let row, colEnv, colValue, colFrequency;

        for (let key in predictions) {
            // write the predictions in the popup
            divPredictions.append(
                capitalizeFirstLetter(key.split("_").join(" ")) + ': '
                + predictions[key]["most_frequent"]
                + " (" + predictions[key]["count_frequent"] + " / 7)"
            ).append($('<br>'));
            // write the predictions in the Excel file
            row = $("<tr>");
            colEnv = $("<td>").html(capitalizeFirstLetter(key.split("_").join(" ")));
            colValue = $("<td>").html(predictions[key]["most_frequent"]);
            colFrequency = $("<td>").html("(" + predictions[key]["count_frequent"] + " / 7)");
            row.append(colEnv, colValue, colFrequency)
            tablePredictions.append(row);
        }

        $(document).on('click', '.downloadPopup', function () {
        // $(".downloadPopup").on("click", function() {
            // e.preventDefault();
            tablePredictions.table2excel({
                type: 'xls',
                filename: 'predictions_' + code_iris + '.xls',
                preserveColors: true
            });
        });

        let popup = old_popup + divPredictions[0].outerHTML; // update the popup content with the old popup and the predictions
        layer.bindPopup(popup);
        layer.bringToFront();
        layer.openPopup();

        $("#selectAlgorithmTooltip")
            .val(algorithm_name) // must be after binding the popup to be effective
            .on("click", function() {
                predict(code_iris, $("#selectAlgorithmTooltip option:selected").val(), old_popup, layer, e);
            }); // keep the same begin (name, code, list of algorithm) and add predictions when clicking on the select

    }

}

/**
 * Add IRIS layer from GeoJSON data
 * @param {geojson} geojson .
 * @param {event} events .
 * @param {object} style .
 * @param {string} typeMethod . (specify the method which caused this display, ex "searchBounds", "searchCode", "searchName")
 */
function addLayerFromGeoJSON(geojson, events, style, typeMethod){
    if(irisLayer != null) { // if already displayed iris, remove them
        removeLayer(irisLayer);
    }
    if(geojson && geojson.length > 0) {
        irisLayer = new L.geoJSON(geojson, {onEachFeature: events});
        irisLayer.setStyle(style);
        irisLayer.addTo(map);
        if(typeMethod !== "searchBounds") // if searchBounds (zoom), fitBounds() will decrease the zoom, thus reloading searchBounds...
            map.fitBounds(irisLayer.getBounds()); // zoom on the displayed iris
    }
    return irisLayer;
}

/**
 * Add an IRIS to the list of IRIS to predict (feature to predict the environment of many IRIS)
 * @param e an event
 */
function addIrisToList(e) {
    let code_iris = e.target.feature.properties.CODE_IRIS;
    let index_code = selected_codes.indexOf(code_iris);
    if(index_code === -1) {
        // the IRIS is not added yet
        selected_codes.push(e.target.feature.properties.CODE_IRIS);
        selected_iris.push(e.target.feature);
        e.target.setStyle({fillColor: "green"});
    } else {
        selected_codes.splice(index_code, 1);
        selected_iris.splice(index_code, 1);
        e.target.setStyle({fillColor: "#3388ff", fillOpacity: 0.2, fillStroke: "#3388ff", strokeWidth: 1});
    }

    $("#addedIrisDiv").empty();
    for(var key of selected_codes) {
        $("#addedIrisDiv").append("Neighbourhood " + key + " is selected.</br>")
    }
}

/**
 * Add tooltip, mouseover, mouseout and click events to the (features of) IRIS layer
 * @param {number} feature .
 * @param {layer} layer .
 */
function neighbourhoodEvents(feature, layer) {
	layer.on({
		// mouseover: highlightFeature,
		// mouseout: resetHighlight,
        contextmenu: addIrisToList,
        click: displayMinimalPopup
	});
}

$("#btnPredictSelectedIris").on("click", function() {
    let selected_algorithm = $("#selectManyIrisAlgo option:selected").val();

    if(selected_algorithm === "undefined" || selected_algorithm === undefined || selected_algorithm === "Algorithm"){
        alert("You have not selected any algorithm. Please select an algorithm that will be used to predict the environment.");
    } else if(selected_iris.length <= 0) {
        alert("You have not selected any neighbourhood. Please select neighbourhoods on the map by right-clicking on it.");
    } else {
        predict(selected_codes, selected_algorithm);
        //selected_iris = [];
        //selected_codes = []; // reset to make new predictions
    }
});

/**
 * Allow the validation of input fields using "enter key".
 * @param event the event gathered (enter key pressed)
 */
function eventValidateButton(event) {
    // a generic method to allow the validation of input fields using "enter key"
    if(event.keyCode == 13)  // enter key has been pressed and released, back to parent and click on the <button>
        event.target.parentNode.querySelector("button").click();
}


// updating values of printed parameters
$("#inputZoomLevel").val(DEFAULT_ZOOM_LEVEL_MIN_FOR_DISPLAY);

// linking elements (buttons, body) to events
$("#boutonRechercherCode").click(getNeighbourhoodFromCode);
$("#inputCode").keyup(eventValidateButton);
$("#boutonRechercherNom").click(getNeighbourhoodFromName);
$("#inputName").keyup(eventValidateButton);
$("#boutonEffacer").click(removeLayer);
$(document).ready(initialize); // when the page is loaded, initialize the map

$("#boutonEnableDisableZoom").click(function() {
    if($("#inputZoomLevel").prop("disabled")) {  // enable the zoom input
        $("#boutonEnableDisableZoom > img").attr("src", "./static/css/open-iconic-master/svg/lock-unlocked.svg");
        $("#inputZoomLevel").removeAttr("disabled"); // prop({disabled: false});
    }
    else {  // disable the zoom input
        $("#boutonEnableDisableZoom > img").attr("src", "./static/css/open-iconic-master/svg/lock-locked.svg");
        $("#inputZoomLevel").prop("disabled", true);
    }
});
