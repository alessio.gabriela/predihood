// let classifiers = getClassifiers();
addClassifiers("#selectAlgorithm");
let current_parameters = null; // parameters' dict from server
let generalParameters = ["class_weight", "cv", "kernel", "max_iter", "memory", "metric", "n_components", "n_jobs", "pos_label", "random_state", "scoring", "verbose", "warm_start"]; // parameters that are not specific to classifiers
let trainPercentage = $("#trainPercentage").val(); // to update test percentage depending on train percentage
let testPercentage = $("#testPercentage").val(); // to update train percentage depending on test percentage
let request_run = null; // the request send to the server (with classifier and its parameters)
const MAX_PARAMETERS = 2;
let preferred_language_algo = get_preferred_language();


// get parameters of the selected classifier and display them in the interface.
$("#selectAlgorithm").change(function () {
    let algorithm_name = $(this).children("option:selected").val();
    getParameters(algorithm_name);
    $("#tuningSection").css("visibility", "visible");
    addVariables(); // add variables to the select #removeLessRepresentative
});

// update up/down symbols when hide or show parameters in the tuning section.
for (let param_section of ["#specificParameters", "#commonParameters"]) {
    $(param_section + "Title").on("click", function () {
        if ($(param_section).css("display") === "none") {
            $(param_section).css("display", "block");
            $(param_section + "Title").addClass("fa-angle-double-up").removeClass("fa-angle-double-down");
        } else if ($(param_section).css("display") === "block") {
            $(param_section).css("display", "none");
            $(param_section + "Title").addClass("fa-angle-double-down").removeClass("fa-angle-double-up");
        }
    });
}

// test percentage input is updated when train input is changed
$("#trainPercentage")
    .change(function () {
        let difference = parseInt(trainPercentage) - parseInt($(this).val());
        let value_test = parseInt($("#testPercentage").val()) + difference;
        $("#testPercentage").val(value_test);
        trainPercentage = $("#trainPercentage").val();
        testPercentage = $("#testPercentage").val();
    })
    .on("click", function (event) {
        event.preventDefault();
    }); // prevent user from clicking on the input

// train percentage input is updated when test input is changed
$("#testPercentage")
    .change(function () {
        let difference = parseInt(testPercentage) - parseInt($(this).val());
        let value_train = parseInt($("#trainPercentage").val()) + difference;
        $("#trainPercentage").val(value_train);
        trainPercentage = $("#trainPercentage").val();
        testPercentage = $("#testPercentage").val();
    })
    .on("click", function (event) {
        event.preventDefault();
    }); // prevent user from clicking on the input

// run the classifier with specified parameters and display results in the results section.
$("#runBtn").click("on", function () {
    document.body.style.cursor = 'wait';
    $(".wrapperTable input[type='checkbox']:not(:checked)").each(function () {
        $(this).parent().parent().empty(); // remove tables that are not checked in the interface
    });
    let userParameters = {};
    let chosen_clf = $("#formAlgorithm")[0].elements[0].value;
    for (let key in $("#formParameters")[0].elements) {
        let elem = $("#formParameters")[0].elements[key];
        if (parseInt(key) === undefined || isNaN(parseInt(key))) {
            continue;
        } // key is not an element of the form

        if (elem.value !== current_parameters[elem.title]["default"] && elem.value !== "") { // get only parameters filled by user
            let label_name = elem.title;
            let val = elem.value;
            if (elem.type === "text") { // input with text type
                if (elem.title.includes("int") && parseInt(elem.value)) {
                    val = parseInt(elem.value)
                } else if (elem.title.includes("float") && parseFloat(elem.val)) {
                    val = parseFloat(elem.value)
                }
                userParameters[label_name] = val;
            } else if (elem.type === "number") { // input with number type
                userParameters[label_name] = parseFloat(elem.value);
            } else if (elem.type === "checkbox") { // input with checkbox type
                userParameters[label_name] = elem.checked;
            }
        }
    }
    userParameters["train_size"] = $("#trainPercentage")[0].valueAsNumber;
    userParameters["test_size"] = $("#testPercentage")[0].valueAsNumber;
    userParameters["remove_outliers"] = $("#removeOutliers").prop("checked");
    userParameters["remove_under_represented"] = $("#removeLessRepresentative option:selected").val();
    userParameters["add_manual_iris"] = false; // $("#addAssessmentIris").prop("checked");

    console.log(userParameters);

    request_run = $.ajax({
        "type": "GET",
        "url": "/run-classifier",
        data: {
            "clf": chosen_clf,
            "parameters": JSON.stringify(userParameters)
        },
        success: function (result) {
            // each result is displayed with :
            //   - a checkbox to keep the results available in the next run
            //   - the results table, highlighted cells are best means for each EV
            //   - the list of parameters associated to the results
            //   - the mean for this classifier (all EV combined)
            if(result["message"] === "KO") {
                alert("Your train_size or test_size parameter was set badly. The algorithm has been run with default parameters which are 80% for train_size and 20% for test_size.")
            }
            let keep = $("<label class='h5'><input type='checkbox' style='margin-right: 1rem;'/>" + chosen_clf + "</label>");
            let table = $("<table id='tableToExport'>")
                .addClass("table table-hover table-responsive")
                .append($("<tbody>"));
            let results = result["results"]

            // header of table: None, 10, 20, ..., 100, Mean
            let header = $("<tr>")
            header.append("<th></th>");
            if(preferred_language_algo === "fr") {
                header.append("<th title='Précision obtenue avec tous les indicateurs'><i>I</i></th>")
                for (let key of result["tops_k"]) { header.append("<th title='Précision obtenue avec la liste de "+key+" indicateurs'>" + key + "</th>") } // adding header with tops-k
                header.append("<th title=\"Précision moyenne obtenue pour la variable d'environnement\">Moyenne</th>")
            } else {
                header.append("<th title='Accuracy obtained with all indicators'>I</th>")
                for (let key of result["tops_k"]) { header.append("<th title='Accuracy obtained by list with "+key+" indicators'>" + key + "</th>") } // adding header with tops-k
                header.append("<th title='Mean accuracy for the environment variable'>Mean</th>")
            }
            table.append(header);
            console.log(results)
            // content of table with computed accuracies
            for (let key in results) { // iterating over env variables
                let row = $("<tr>");
                let env = results[key];
                let max = getMaxValueDict(results[key]["accuracies"], env["accuracy_none"]);
                row.append("<td>" + capitalizeFirstLetter(key.split("_").join(" ")) + "</td>")

                let col = $("<td>").text(env["accuracy_none"].toFixed(2) + "%")
                if (env["accuracy_none"] === max) {
                    col.css("background-color", "#71dd8a")
                }
                row.append(col);

                for (let top_k in env["accuracies"]) { // iterating over top-k for each EV
                    let col = $("<td>").text(env["accuracies"][top_k].toFixed(2) + "%");
                    if (env["accuracies"][top_k] === max) {
                        col.css("background-color", "#71dd8a");
                    }
                    row.append(col);
                }
                row.append("<td>" + env["mean"].toFixed(2) + "%</td>");
                table.append(row)
            }

            // download icon
            let download;
            if(preferred_language_algo === "fr") {
                download = $("<i class='fas fa-download' style='margin-left: 1rem;' title='Exporter cette table comme un fichier Excel.'></i>")
            } else {
                download = $("<i class='fas fa-download' style='margin-left: 1rem;' title='Export this table as an Excel file.'></i>")
            }

            download
                .addClass("colorfulIcon")
                .on("click", function (e) {
                    e.preventDefault();
                    $("#tableToExport").table2excel({
                        type: 'xls',
                        filename: chosen_clf + '.xls',
                        preserveColors: true
                    });
                });
            let containing_table = $("<div>").prop("class", "wrapperTable");
            containing_table.append(keep).append(download).append(table);

            // list of parameters used to have the current results
            let params = "";
            for (let elem in current_parameters) {
                if (elem in userParameters) {
                    params += "<i>" + elem + "</i>: " + userParameters[elem] + " ; "; // adding user value
                } else {
                    params += "<i>" + elem + "</i>: " + current_parameters[elem]["default"] + " ; ";
                } // adding default value
            }
            containing_table.append(params);

            // Mean accuracy for this classifier
            let mean_clf = 0;
            for (let env in results) {
                mean_clf += results[env]["mean"];
            }
            mean_clf /= Object.keys(results).length;
            if(preferred_language_algo === "fr") {
                containing_table.append("<br/> <b>Moyenne de cet algorithme : </b>" + mean_clf.toFixed(2) + "%");
            } else {
                containing_table.append("<br/> <b>Mean for this classifier: </b>" + mean_clf.toFixed(2) + "%");
            }

            // append all to HTML
            $("#resultsDiv").append(containing_table);
            document.body.style.cursor = 'default';
        },
        error: function (result, textStatus, errorThrown) {
            console.log(errorThrown);
            alert("something went wrong while training. Please check your parameters<br>" + textStatus);
            document.body.style.cursor = 'default';
        }
    });

    return false; // do not reload
});

// empty results when clicking on the trash icon
$("#clearResults").on("click", function () {
    // empty div and add title + "clear all" button
    $("#resultsDiv")
        .empty()
        .append("<h2 title='Results of the tuned classifier'>Results<i class='far fa-trash-alt' id='clearResults' title='Clear all results' style='color: red; font-size: 22px; padding-left: 0.5rem' ></i></h2>");
});

/**
 * Adds the parameter in the interface, with label and input.
 * @param {string} label The name of the parameter.
 * @param {string} content The value of the parameter (default value).
 * @param {string} type The type of the input (i.e. str, int, float, bool or None).
 * @param {string} description The description of the parameter. It corresponds to the first sentence in the doc (sklearn) for the parameter.
 * @param {boolean} hidden A boolean that indicates if the field is hidden or not (because we display only 5 parameters by default).
 */
function addElement(label, content, type, description, hidden) {
    // adds something like :
    // <li class="nav-item">
    //   <div class="nav-link">
    //     <i class="fa fa-question-circle mr-3 text-primary fa-fw"></i>
    //     parameter
    //     <input type="text" placeholder="">
    //   </div>
    // </li>
    let element_parameter = $("<li>").addClass("nav-item");
    let div_parameter = $("<div>").addClass("nav-link")
        .append("<i class='fa fas fa-question-circle mr-3 text-primary fa-fw' title='" + type + "'></i>")
        .append(label)
        .prop("title", description);

    let input;
    if (content === "None" || type.includes("str")) {
        input = $("<input>")
            .attr('placeholder', content);
    } else {
        if (type.includes("int")) {
            // INT VALUE -> RANGE
            let int_value = parseInt(content)
            input = $("<input>")
                .val(int_value)
                .attr('type', 'number')
                .attr('min', 0)
                .attr('step', 1);
            if (label === "max_iter") {
                input.attr('min', -1); // specific case for max_iter parameter
            }
        } else if (type.includes("float")) {
            // FLOAT VALUE -> RANGE
            let float_value = parseFloatComplex(content);
            input = $("<input>")
                .val(float_value)
                .attr('type', 'number')
                .attr('min', float_value)
                .attr('step', (float_value === 0.0 || float_value === "0.0") ? (0.1) : (float_value)); // step is the default value
        } else if (type.includes("bool")) {
            // BOOLEAN VALUE -> CHECKBOX
            if (content === "True") {
                input = $("<input>").attr('type', 'checkbox').prop("checked", true);
            } else {
                input = $("<input>").attr('type', 'checkbox');
            }
        } else {
            // DEFAULT CASE -> INPUT
            input = $("<input>")
                .attr('placeholder', content);
        }
    }
    input.attr('title', label);
    div_parameter.append("<br>").append(input);
    let rowClass = (hidden === true) ? "row hiddenParam" : "row";
    element_parameter.append(div_parameter).addClass(rowClass);
    generalParameters.includes(label) ? $("#commonParameters").append(element_parameter) : $("#specificParameters").append(element_parameter); // append row to the right div
    hidden ? element_parameter.hide() : element_parameter.show(); // display or not the row
}

/**
 * Adds all parameters that are in jsonData parameter by adding a div for the label and one for the input.
 * @param{json}    jsonData    the dictionary with all parameters to add in the interface.
 */
function addParameters(jsonData) {
    $("#specificParameters").empty(); // empty div before adding new labels and inputs
    $("#commonParameters").empty();
    let stepCommon = 0;
    let stepSpecific = 0;
    for (let element in jsonData) {
        if (generalParameters.includes(element)) {
            addElement(element, jsonData[element]["default"], jsonData[element]["types"], jsonData[element]["description"], (stepCommon > MAX_PARAMETERS));
            stepCommon++;
        } else {
            addElement(element, jsonData[element]["default"], jsonData[element]["types"], jsonData[element]["description"], (stepSpecific > MAX_PARAMETERS));
            stepSpecific++;
        }
    }

    addShowMoreLess();
}

/**
 * Adds a "show more" or "show less" link in the classifier's parameters section.
 */
function addShowMoreLess() {
    let link_show_more = $("<a>").attr("id", "showMoreLessParams").attr("class", "seeMore").attr("href", "#")
    if(preferred_language_algo === "fr") {
        link_show_more.text("Afficher plus...").attr("title", "Afficher plus de paramètres")
    } else {
        link_show_more.text("Show more...").attr("title", "Show more parameters")
    }
    $("#specificParameters").append(link_show_more);

    $('#showMoreLessParams').click(function () {
        $(this).toggleClass('seeMore');
        if ($(this).hasClass('seeMore')) {
            if(preferred_language_algo === "fr") {
                $(this).text('Afficher plus').attr("title", "Afficher plus de paramètres");
            } else {
                $(this).text('Show more').attr("title", "Show more parameters");
            }
            $("#specificParameters li.hiddenParam").each(function () {
                $(this).hide(); // hide other parameters
            });
        } else {
            if(preferred_language_algo === "fr") {
                $(this).text('Afficher moins').attr("title", "Afficher moins de paramètres");
            } else {
                $(this).text('Show less').attr("title", "Show less parameters");
            }
            $("#specificParameters li.hiddenParam").each(function () {
                $(this).show(); // show all parameters
            });
        }
    });
}

/**
 * Get parameters for the given algorithm.
 * @param {string}    name    The name if the selected algorithm.
 */
function getParameters(name) {
    $.ajax({
        type: "GET",
        url: "/get-parameters",
        data: {
            'name': name
        },
        contentType: 'application/json;charset=UTF-8',
        success: function (result) {
            if(result instanceof String) {
                alert(result)
            } else {
                current_parameters = result; // store current parameters (for the chosen classifier) in a global variable
                let parameters = result;
                if (Object.keys(parameters).length > 0) {
                    // at least one key
                    addParameters(parameters);
                } else {
                    // nothing to append, empty the div (no selection)
                    $("#tuningSection").css("visibility", "hidden");
                }
            }
        },
        error: function (result, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}

