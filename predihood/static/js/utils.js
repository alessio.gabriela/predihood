/*
** Non-cartographic and ajax-related functions
*/

// parameters
let styleDisplayedNeighbourhoods = {style : {color: "#000000", fillColor: "#000000", fillOpacity: 0}};
let classifiers = getClassifiers();
let variables = getEnvironmentVariables();

function refreshMessages() {
    $("#zoneMessages").load(location.href + " #zoneMessages");  // reload #zoneMessages to display messages
}


/**
  * Call the AJAX request to get a neighbourhood from a given code
  * @param{object} e : an EventObject describing the event
  * @return a GeoJSON object containing a field 'status' and a field 'geojson' (list of neighbourhood)
**/
function getNeighbourhoodFromCode(e) {
	console.log("Executing getNeighbourhoodFromCode()...");
	$.ajax({
		type: "GET",
		url: "/search-code",
		data: {
			'neighbourhood_code': $("#inputCode").val()
		},
		contentType: 'application/json;charset=UTF-8',
		success: function(result){
			jsonResult = JSON.parse(result);
            neighbourhoodLayer = addLayerFromGeoJSON(jsonResult['geojson'], neighbourhoodEvents, styleDisplayedNeighbourhoods, "searchCode");
            refreshMessages();
		},
		error: function(result, textStatus, errorThrown) {
            console.log(errorThrown);
            refreshMessages();
		}
	});
}

/**
  * Call the AJAX request to get a neighbourhood from a given search query (i.e., name of neighbourhood or city)
  * @param{object} e : an EventObject describing the event
  * @return a GeoJSON object containing a field 'status' and a field 'geojson' (list of neighbourhoods)
**/
function getNeighbourhoodFromName(e) {
	console.log("Executing getNeighbourhoodFromName()...");
	$.ajax({
		type: "GET",
		url: "/search-name",
		data: {
			'querySearch': $("#inputName").val()
		},
		contentType: 'application/json;charset=UTF-8',
		success: function(result){
			let jsonResult = JSON.parse(result);
			console.log(jsonResult);
            neighbourhoodLayer = addLayerFromGeoJSON(jsonResult['geojson'], neighbourhoodEvents, styleDisplayedNeighbourhoods, "searchName");
            refreshMessages();
		},
		error: function(result, textStatus, errorThrown) {
            console.log(errorThrown);
            refreshMessages();
		}
	});
}


/**
  * Call the AJAX request to retrieve neighbourhood in the bounded area
  * @param{array} areaBounds : a pair of coordinates (latitude, longitude) delimiting a rectangular area
  * @return a GeoJSON object containing a field 'status' and a field 'geojson' (list of neighbourhoods)
**/
function getNeighbourhoodsForBounds(areaBounds) {
	console.log("Executing getNeighbourhoodsForBounds()...");
	$.ajax({
		type: "GET",
		url: "/get-neighbourhoods-polygon",
		data: {
			'lat1': areaBounds["_southWest"]["lat"],  // latitude southwest point
			'lng1': areaBounds["_southWest"]["lng"],  // longitude southwest point
			'lat2': areaBounds["_northEast"]["lat"],  // latitude northeast point
			'lng2': areaBounds["_northEast"]["lng"],  // longitude northeast point
		},
		contentType: 'application/json;charset=UTF-8',
		success: function(result){
			jsonResult = JSON.parse(result);
            neighbourhoodLayer = addLayerFromGeoJSON(jsonResult['geojson'], neighbourhoodEvents, styleDisplayedNeighbourhoods, "searchBounds");
            refreshMessages();
		},
		error: function(result, textStatus, errorThrown) {
			/*
			$("#loading").hide();
			if(document.getElementById("danger") == null) {
                $("#informations").append("<div id=\"danger\" class=\"alert alert-danger\">La recommandation a rencontré un problème, veuillez recommencer...</div>");
            }
            */
            console.log(errorThrown);
            refreshMessages();
		}
	});
}

/**
  * Call the AJAX request to count the number of neighbourhoods in the bounded area
  * @param{array} areaBounds : a pair of coordinates (latitude, longitude) delimiting a rectangular area
  * @param{object} callback : a method which is executed
  * @return a GeoJSON object containing a field 'status' and a field 'nbIris'
**/
function countIrisForBounds(areaBounds, callback) {
	console.log("Executing countIrisForBounds()...");
	$.ajax({
		type: "GET",
		url: "/count-neighbourhoods-polygon",
		data: {
			'lat1': areaBounds["_southWest"]["lat"],  // latitude southwest point
			'lng1': areaBounds["_southWest"]["lng"],  // longitude southwest point
			'lat2': areaBounds["_northEast"]["lat"],  // latitude northeast point
			'lng2': areaBounds["_northEast"]["lng"],  // longitude northeast point
		},
		contentType: 'application/json;charset=UTF-8',
		success: function(result){
			jsonResult = JSON.parse(result);
			nbIris = jsonResult['nbIris'];
			console.log("nb neighbourhood in area = " + nbIris);
            return jsonResult['nbIris'];
		},
		error: function(result, textStatus, errorThrown) {
            console.log(errorThrown);
		}
	});
}

/**
 * Get the maximum value between values of dict.
 * @param dict a dict object.
 * @param min_value if the max is lower than this value, min_value is returned.
 * @returns {*} the highest value in the dict (among the values, not keys).
 */
function getMaxValueDict(dict, min_value) {
	let max = dict[Object.keys(dict)[0]]; // max value is by default the first value
	for(let key in dict) {
		if(dict[key] > max) {
			max = dict[key];
		}
	}

	return (max > min_value) ? max : min_value;
}

/**
 * Parse string containing float in exponent representation in float.
 * @param str A string containing a float with an exponent, e.g. 3e-7
 * @returns {number} The float number of type float, e.g. 0.0000003
 */
function parseFloatComplex(str) {
	let exponent = "e-";
	if(typeof(str) === 'string' && str.includes(exponent)) {
		// ex. 3e-7
		let number = parseInt(str.substring(0, str.indexOf(exponent)));
		let exp = parseInt(str.substring(str.indexOf(exponent)+exponent.length, str.length+1));
		let i = 0;
		while(exp > 0) {
			i += 1;
			number = number / 10
			number = number.toFixed(i); // to avoid
			exp -= 1;
		}
		return number;
	} else if(typeof(str) === 'string' && str.includes(".") && str.substring(str.indexOf(".")+1, str.length+1) === "") {
		// 0.
		let number = parseInt(str.substring(0, str.indexOf(".")));
		return number.toFixed(1);
	} else {
		// other cases
		return parseFloat(str);
	}
}

/**
 * Get a list containing the environment variables.
 * @returns {[]} a list containing the environment variables' names
 */
function getEnvironmentVariables() {
	let env_var = null;
	$.ajax({
		type: "GET",
		url: "/get-variables-values",
		async: false,
		contentType: 'application/json;charset=UTF-8',
		success: function(result){
            env_var = JSON.parse(result)
		},
		error: function(result, textStatus, errorThrown) {
            console.log(errorThrown);
		}
	});
	return env_var;
}

/**
 * Get the preferred language that have been selected by the user.
 * @return {string} a string containing the name of the preferred language.
 */
function get_preferred_language() {
	let chosen_language = undefined;

	$.ajax({
		type: "GET",
		url: "/get-preferred-language",
		async: false,
		contentType: 'application/json;charset=UTF-8',
		success: function(result) {
			chosen_language = JSON.parse(result)["preferred_language"]
		},
		error: function(result, textStatus, errorThrown) {
			console.log(errorThrown)
		}
	})
	return chosen_language
}

/**
 * Capitalize first letter of a string.
 * @param string the string on which the first letter will be capitalized
 * @returns {string} the string with the first letter capitalized
 */
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 * Add all classifiers in the select.
 */
function addClassifiers(id_element) {
	// $(id_element).append($("<option>").prop("value", "undefined").text("---"));
    for (let classifier of classifiers) {
    	let option = $("<option>").prop("value", classifier).text(classifier);
        $(id_element).append(option);
    }
}

function addVariables() {
	console.log(variables);
	for(let variable in variables) {
		let option = $("<option>").prop("value", variable).text(variable);
		console.log(option);
		$("#removeLessRepresentative").append(option);
	}
}

/**
 * Get the list of available classifiers, available in AVAILABLE_CLASSIFIERS (in config.py).
 * @returns {[]} a list containing the names of available classifiers.
 */
function getClassifiers() {
    let classifiers = [];
    $.ajax({
        "type": "GET",
        "url": "/get-classifiers",
        "async": false,
        contentType: 'application/json;charset=UTF-8',
        success: function (result) {
            classifiers = result["classifiers"];
        },
        error: function (result, textStatus, errorThrown) {
            console.log(errorThrown)
        }
    });
    console.log(classifiers)
    return classifiers;
}
