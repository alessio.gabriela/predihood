#!/usr/bin/env python
# encoding: utf-8
"""
Unit tests for Predihood.
"""

import os
import pandas as pd
import unittest

from predihood import config, model
from predihood.config import FOLDER_DATASETS, ENVIRONMENT_VALUES, FOLDER_HIL
from predihood.utility_functions import check_train_test_percentages, intersection, union, similarity, \
    get_most_frequent, address_to_code, address_to_city, indicator_full_to_short_label, indicator_short_to_full_label, \
    get_classifier, set_classifier, signature_of_algorithm, add_assessment_to_file, load_dataset_config, \
    create_config_dictionaries
from sklearn.neighbors import KNeighborsClassifier


class TestCase(unittest.TestCase):
    """
    A class for Predihood unit tests.
    """

    """
    Before running the tests, make sure that the database is running correctly.
    """
    def setUp(self):
        host = os.environ.get('HOST', '127.0.0.1')  # database server (docker container)
        port = int(os.environ.get('PORT', 27017))  # database server port
        dataset_config_file = "datasets/hil/config.json"
        dataset_config = load_dataset_config(dataset_config_file)
        config.CURRENT_CONFIG = "hil"
        create_config_dictionaries(dataset_config)
        model.connect(host, port, dataset_config['DATABASE_NAME'], dataset_config['COLLECTION_NAME'])

    """
    Test if converting an address into a code gives the correct code.
    Test if converting an address not located in France returns None.
    """
    def test_address_to_code(self):
        address = "46 avenue Victor Hugo Tassin"
        code = address_to_code(address)
        assert code == "692440201"

        address = "Westminster, London SW1A 0AA, United Kingdom"
        code = address_to_code(address)
        assert code is None

    """
    Test if converting an address into a city gives the correct city.
    """
    def test_address_to_city(self):
        address = "46 avenue Victor Hugo Tassin"
        city = address_to_city(address)
        assert city == "Tassin-la-Demi-Lune"

    """
    Test if converting a full label of an indicator into its short label gives the correct short label.
    """
    def test_indicator_full_to_short_label(self):
        full_label = "Pop 11-17 ans en 2014 (princ)"
        short_label = indicator_full_to_short_label(full_label)
        assert short_label == "P14_POP1117"

    """
    Test if converting a short label of an indicator into its full label gives the correct full label.
    """
    def test_indicator_short_to_full_label(self):
        short_label = "P14_POP1117"
        full_label = indicator_short_to_full_label(short_label)
        assert full_label == "Pop 11-17 ans en 2014 (princ)"

    """
    Test if selecting a classifier gives the correct object.
    """
    def test_get_classifier(self):
        classifier_name = "KNeighbors Classifier"
        classifier = get_classifier(classifier_name)
        # classifier == KNeighborsClassifier() fails because they are different objects
        assert type(classifier) == type(KNeighborsClassifier())

    """
    Test if setting the parameters of a classifier changes its internal parameters.
    """
    def test_set_classifier(self):
        classifier_name = "KNeighbors Classifier"
        classifier = get_classifier(classifier_name)
        assert classifier.n_neighbors == 5  # default is 5

        parameters = {"n_neighbors": 10}
        classifier = set_classifier(classifier, parameters)
        assert classifier.n_neighbors == 10

    """
    Test if the signature function is able to pick correct parameters.
    """
    '''
    def test_signature(self):
        classifier_name = "Random Forest Classifier"
        classifier_signature, _ = signature_of_algorithm(classifier_name)
        print(classifier_signature)
        assert "n_estimators" in classifier_signature
        assert classifier_signature["n_estimators"]["types"] == ["int"]
        assert classifier_signature["n_estimators"]["default"] == "100"
    '''

    """
    Test if train and test sizes are correctly changed if needed.
    """
    def test_train_test_percentages(self):
        train_size, test_size, _ = check_train_test_percentages(0.7, 0.3)
        assert train_size == 0.7
        assert test_size == 0.3

        train_size, test_size, _ = check_train_test_percentages(40, 60)
        assert 0 < train_size < 1
        assert 0 < test_size < 1
        assert train_size + test_size == 1

        train_size, test_size, _ = check_train_test_percentages(0.7, 0.4)
        assert 0 < train_size < 1
        assert 0 < test_size < 1
        assert train_size + test_size == 1

    """
    Test if intersection of two lists gives the correct output.
    """
    def test_intersection(self):
        l1 = [1, 2, 3]
        l2 = [2, 3, 4]
        result = intersection(l1, l2)
        assert result == [2, 3]

    """
    Test if union of two lists gives the correct output.
    """
    def test_union(self):
        l1 = [1, 2, 3]
        l2 = [2, 3, 4]
        result = union(l1, l2)
        assert result == [1, 2, 3, 4]

    """
    Test if a given value is correctly compared to already added values by checking the returned index.
    """
    def test_similarity(self):
        value = "Countyside"
        lst = [["Houses", "House"], ["Countryside", "Countrysid"], ["Green areas"]]
        index = similarity(value, lst)
        assert index == 1

        value = "Countyside"
        lst = [["Countrysidee", "Coutysid"]]
        index = similarity(value, lst)
        assert index == -1

        value = "Countryside"
        lst = [["Countryside", "Countrysid"]]
        index = similarity(value, lst)
        assert index == -2

    """
    Test if the most frequent function gives the correct value and count correctly the number of occurrences.
    """
    def test_get_most_frequent(self):
        lst = ["Houses", "Houses", "Towers", "Housing estate", "Houses"]
        result = get_most_frequent(lst)
        assert result["most_frequent"] == "Houses"
        assert result["count_frequent"] == 3

    """
    Test if an IRIS is correctly added to the dataset and if it is not appended twice.
    """
    def test_add_assessment_to_file(self):
        neighbourhood_code = "692440201"
        values = ["Houses", "Housing", "Green areas", "Peri-urban", "West", "Upper middle"]
        add_assessment_to_file(neighbourhood_code, values)

        neighbourhood_code = "692440201"
        values = ["Houses", "Housing", "Green areas", "Peri-urban", "West", "Upper middle"]
        result = add_assessment_to_file(neighbourhood_code, values)
        assert result == "iris already assessed"

    """
    Test if values used in dataset are the same than the one declared by social science researchers.
    """
    def test_values_dataset(self):
        filename = os.path.join(FOLDER_HIL, "data_density.csv")
        dataset = pd.read_csv(filename)
        values_for_variable1 = set([value for key, value in config.ENVIRONMENT_VALUES["variable1"].items()])

        assert set(dataset["variable1"].tolist()) == values_for_variable1


if __name__ == "__main__":
    unittest.main(verbosity=3)  # run all tests with verbose mode
