"""
Methods for predicting a single neighbourhood or for a set of neighbourhoods.

**Note:** when exporting the results as a XLS file, your tablesheet software may produce a _warning about incompatible file format and extension_ (at least on Excel for MacOS). This warning is due to the HTML to XLS converter, and you can _safely open the XLS file_.
"""

# eternal hack for python imports (here to access parent directory)
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

import logging
import predihood.config

from collections import OrderedDict
from predihood.classes.Data import Data
from predihood.classes.Dataset import Dataset
from predihood.classes.MethodPrediction import MethodPrediction
import predihood.config
from predihood.config import TRAIN_SIZE, TEST_SIZE, ENVIRONMENT_VALUES
from predihood.selection import retrieve_lists
from predihood.utility_functions import check_train_test_percentages, get_most_frequent, get_env_name_translation, \
    get_env_value_translation
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.exceptions import ConvergenceWarning
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.neighbors import KNeighborsClassifier, NearestCentroid
from sklearn.svm import SVC
from sklearn.utils._testing import ignore_warnings

log = logging.getLogger(__name__)


# define classifiers used in the experimental validation
CLASSIFIERS = [
    LogisticRegression(penalty="l2"),
    KNeighborsClassifier(n_neighbors=30),
    RandomForestClassifier(n_estimators=300),
    SVC(kernel="rbf"),
    SGDClassifier(),
    NearestCentroid(),
    AdaBoostClassifier()
]


@ignore_warnings(category=ConvergenceWarning)
def compute_all_accuracies(data, clf, train_size, test_size, remove_outliers=False, remove_underrepresented=False):
    """
    Compute accuracies for each EV and each list with the given classifier.

    Args:
        data: a Data object that contains assessed neighbourhoods and their attributes
        clf: an object which is a classifier (with `fit` and `predict` methods)
        train_size: an integer corresponding to the size of the training sample
        test_size: an integer corresponding to the size of the test sample
        remove_outliers: True to remove from the dataset neighbourhoods that are detected as outliers, False else
        remove_underrepresented: True to remove neighbourhoods that are in the countryside to avoid bias while predicting, False else

    Returns:
        a dictionary of results for each EV and each list of selected indicators
    """
    log.info("... Computing accuracies ...")
    train_size, test_size, message = check_train_test_percentages(train_size, test_size)

    data_not_filtered = Data(normalization=predihood.config.NORMALIZATION, filtering=False, add_assessment=data.add_assessment)
    data_not_filtered.init_all_in_one()

    lists = retrieve_lists(add_assessment=data.add_assessment)
    results = {}
    for j, env in enumerate(predihood.config.ENVIRONMENT_VARIABLES):
        results[get_env_name_translation(env, predihood.config.PREFERRED_LANGUAGE)] = OrderedDict()
        log.debug("--- %s ---", env)
        dataset = Dataset(data_not_filtered, env, selected_indicators=data_not_filtered.indicators, train_size=train_size, test_size=test_size, outliers=remove_outliers, remove_underrepresented=remove_underrepresented, _type='supervised')
        dataset.init_all_in_one()
        # if remove_rural: dataset.remove_rural_neighbourhoods()

        mean_classifier = 0.0
        algo = MethodPrediction(name="", dataset=dataset, classifier=clf)
        algo.fit()
        algo.compute_performance()
        results[get_env_name_translation(env, predihood.config.PREFERRED_LANGUAGE)]["accuracy_none"] = algo.accuracy
        results[get_env_name_translation(env, predihood.config.PREFERRED_LANGUAGE)]["accuracies"] = OrderedDict()
        log.info("accuracy for %s without filtering: %f", env, algo.accuracy)

        for top_k, lst in lists.items():
            dataset = Dataset(data, env, selected_indicators=lst[env], train_size=train_size, test_size=test_size, outliers=remove_outliers, remove_underrepresented=remove_underrepresented, _type='supervised')
            dataset.init_all_in_one()
            # if remove_rural: dataset.remove_rural_neighbourhoods()
            algo2 = MethodPrediction(name='', dataset=dataset, classifier=clf)
            algo2.fit()
            algo2.compute_performance()
            mean_classifier += algo2.accuracy
            results[get_env_name_translation(env, predihood.config.PREFERRED_LANGUAGE)]["accuracies"][str(top_k)] = algo2.accuracy
            log.info("accuracy for %s with %s: %f", env, top_k, algo2.accuracy)
        mean_classifier /= len(CLASSIFIERS)
        results[get_env_name_translation(env, predihood.config.PREFERRED_LANGUAGE)]["mean"] = mean_classifier
        log.info("mean for classifier: %f", mean_classifier)
    results = OrderedDict(results)
    log.info(results)
    return results, message


def predict_one_neighbourhood(neighbourhood_code, data, clf, train_size, test_size, remove_outliers=False, remove_underrepresented="None"):
    """
    Predict the 6 EV for the given neighbourhood, the given data and the given classifier.

    Args:
        neighbourhood_code: a string that contains the code of the neighbourhood (9 digits)
        data: a Data object on which classifier will learn
        clf: an object (classifier) to perform the prediction
        train_size: an integer corresponding to the size of the train sample
        test_size: an integer corresponding to the size of the test sample
        remove_outliers: True to remove from the dataset neighbourhoods that are detected as outliers, False else

    Returns:
        A dictionary containing predictions for each EV.
    """
    train_size, test_size, message = check_train_test_percentages(train_size, test_size)
    lists = retrieve_lists()

    predictions = {}
    for j, env in enumerate(predihood.config.ENVIRONMENT_VARIABLES):
        log.info("predicting for %s", env)
        predictions_lst = []
        for top_k, lst in lists.items():
            dataset = Dataset(data, env, selected_indicators=lst[env], train_size=train_size, test_size=test_size, outliers=remove_outliers, remove_underrepresented=remove_underrepresented, _type="supervised")
            dataset.init_all_in_one()
            algorithm = MethodPrediction(name='', dataset=dataset, classifier=clf)
            algorithm.fit()
            algorithm.predict(neighbourhood_code)
            predicted_value = get_env_value_translation(algorithm.prediction)  # get french translation of the predicted value
            predictions_lst.append(predicted_value)
        predictions[get_env_name_translation(env, predihood.config.PREFERRED_LANGUAGE)] = get_most_frequent(predictions_lst)  # get the most frequent value and the number of occurrences
    print(predictions)  # {'building_type': {'most_frequent': 'Towers', 'count_frequent': 7}, 'building_usage': {'most_frequent': 'Housing', 'count_frequent': 4}, ... }
    return predictions, message


if __name__ == '__main__':

    # Create data
    data = Data(normalization="population", filtering=True)
    data.init_all_in_one()

    # Compute accuracies for each EV and each top-k
    compute_all_accuracies(data, RandomForestClassifier(), TRAIN_SIZE, TEST_SIZE)

    # Predict EV of the "Part-Dieu" IRIS, which is he CBD of Lyon (Central Business District)
    predict_one_neighbourhood("693830301", data, RandomForestClassifier(), TRAIN_SIZE, TEST_SIZE)
