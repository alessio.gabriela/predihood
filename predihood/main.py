#!/usr/bin/env python
# encoding: utf-8
"""
Main Predihood program for launching the [Flask server](http://flask.pocoo.org/) and defining routes to functionalities (search, predict, etc.).
"""

# eternal hack for python imports (here to access parent directory)
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

import json
import logging
import webbrowser
import pandas as pd
from flask import Flask, flash, render_template, request, send_from_directory
from predihood import model
from predihood.classes.Data import Data
from predihood.classifiers_list import AVAILABLE_CLASSIFIERS
import predihood.config  # import FILE_MANUAL_ASSESSMENT_EXPERTS, PREFERRED_LANGUAGE, ENVIRONMENT_VALUES, TOPS_K, translations
from predihood.predict import compute_all_accuracies, predict_one_neighbourhood
from predihood.utility_functions import signature_of_algorithm, get_classifier, set_classifier, add_assessment_to_file, \
    load_dataset_config, create_config_dictionaries
from sklearn.utils._testing import ignore_warnings

log = logging.getLogger(__name__)
dataset_config_file = os.path.join("datasets", "hil", "config.json") # default dataset config file
dataset_config = None  # dataset configuration
app = Flask(__name__)
app.config["JSON_SORT_KEYS"] = False
url = "http://127.0.0.1:8081/"

host = os.environ.get('HOST', '127.0.0.1')  # database server (docker container)
port = int(os.environ.get('PORT', 27017))  # database server port


@app.route('/', defaults={'page': None}, methods=["GET"])
def index(page):
    """
    Render the main page of the interface, i.e. `cartographic-interface.html`.

    Returns:
        The page to display.
    """
    if not model.db.connection_status:  # if no connection, display a flashing message
        flash("Could not connect to the MongoDB database! Check the connection.", "danger")
    if "lang" in request.args:
        predihood.config.PREFERRED_LANGUAGE = request.args["lang"]
    else:
        predihood.config.PREFERRED_LANGUAGE = "en"
    return render_template("index.html", translations=predihood.config.translations, language=predihood.config.PREFERRED_LANGUAGE)


@app.route('/cartographic-interface.html', methods=["GET"])
def get_cartographic_page():
    """
    Render the page of the cartographic interface, i.e. `cartographic-interface.html`.

    Returns:
        The page to display.
    """
    return render_template('cartographic-interface.html', translations=predihood.config.translations,
                           language=predihood.config.PREFERRED_LANGUAGE)


@app.route('/algorithmic-interface.html', methods=["GET"])
def get_algorithms_page():
    """
    Render the page of the algorithmic interface, i.e. `algorithmic-interface.html`.

    Returns:
        The page to display.
    """
    return render_template('algorithmic-interface.html', translations=predihood.config.translations,
                           language=predihood.config.PREFERRED_LANGUAGE)


@app.route('/details-neighbourhood.html', methods=["GET"])
def get_details_neighbourhood():
    """
    Get all information about the given neighbourhood.

    Returns:
        A page that contains all information about the given neighbourhood (descriptive, grouped and raw indicators).
    """
    code_neighbourhood = request.args['neighbourhood_code']
    neighbourhood = model.get_neighbourhood_from_code(code_neighbourhood)
    dict_code_label = model.json_iris_indicator_code_to_label  # model.parse_json_to_dict(model.json_iris_indicator_code_to_label)
    if neighbourhood is None:
        flash("No corresponding neighbourhood for code " + code_neighbourhood + ".", "warning")
    return render_template('details-neighbourhood.html', neighbourhood=neighbourhood, dict_code_label=dict_code_label,
                           translations=predihood.config.translations, language=predihood.config.PREFERRED_LANGUAGE)


@app.route('/get-classifiers', methods=["GET"])
def get_classifiers():
    """
    Get list of available classifiers (stored in AVAILABLE_CLASSIFIERS).

    Returns:
         A list containing the names of the available classifiers.
    """
    available_classifiers = list(AVAILABLE_CLASSIFIERS.keys())
    if available_classifiers is not None:
        return {"classifiers": available_classifiers}
    else:
        return {"error": "The list of classifiers can not be retrieved. You might reload the page."}


@app.route('/get-parameters', methods=["GET"])
def get_parameters():
    """
    Get parameters of the given classifier by give its name.

    Returns:
        A dictionary containing for each parameter its name, its types, its default value, and its definition.
    """
    if 'name' in request.args:
        name = request.args['name']
        parameters, message = signature_of_algorithm(name)
        print(" --- PARAMETERS ---", parameters)
        if parameters != {} and message == "OK":
            return parameters
        else:
            if message == "no_documentation":
                return "There is no documentation for the selected algorithm, thus parameters can not be retrieved."
            elif message == "no_parameters":
                return "There was a problem while retrieving algorithm's parameters. You can check the Python console."
    else:
        return "There is no parameters to get. Be sure to select an algorithm in the list."


@app.route('/run-classifier', methods=["GET"])
@ignore_warnings(category=FutureWarning)
def run_classifier():
    """
    Run classifier on data with the specified parameters.

    Returns:
        The computed accuracies for each EV and each list. The top-k are also returned.
    """
    # 1. get parameters specified by the user
    clf_name = request.args['clf']
    print("clf_name: ", clf_name)
    parameters = json.loads(request.args['parameters'])
    train_size = parameters["train_size"]
    test_size = parameters["test_size"]
    remove_outliers = parameters["remove_outliers"]
    remove_under_represented = parameters["remove_under_represented"]
    add_manual_assessment = parameters["add_manual_iris"]

    # 2. create an instance of the given classifier and tune it with user's parameters
    clf = get_classifier(clf_name)
    clf = set_classifier(clf, parameters)

    # 3. run experiment on data to get accuracies for each EV and each list of selected indicators
    data = Data(normalization=predihood.config.NORMALIZATION, filtering=True, add_assessment=add_manual_assessment)
    data.init_all_in_one()
    accuracies, message = compute_all_accuracies(data, clf, train_size, test_size, remove_outliers, remove_under_represented)
    return {"results": accuracies, "tops_k": predihood.config.TOPS_K, "message": message}


@app.route('/predict-neighbourhood', methods=["GET"])
def predict_neighbourhood():
    """
    Predict the values (e.g., of the six environment variables) of the given neighbourhood.

    Returns:
        Result predictions
    """
    neighbourhood_code_to_predict = request.args['iris_code']
    clf_name = request.args['algorithm_name']
    print(neighbourhood_code_to_predict, clf_name)
    clf = get_classifier(clf_name)

    data = Data(normalization=predihood.config.NORMALIZATION, filtering=True)
    data.init_all_in_one()
    predictions, message = predict_one_neighbourhood(neighbourhood_code_to_predict, data, clf, 0.8, 0.2, remove_underrepresented=predihood.config.VARIABLE_REMOVE_LOW_REPRESENTATIVITY)  # clf
    return {"predictions": predictions, "message": message}


@app.route('/predict-neighbourhoods', methods=["GET"])
def predict_neighbourhoods():
    """
    Predict the values (e.g., of the six environment variables) of each given neighbourhood.

    Returns:
        Result predictions
    """
    neighbourhoods_codes_to_predict = json.loads(request.args['list_codes'])
    clf_name = request.args['algorithm_name']
    print(neighbourhoods_codes_to_predict, clf_name)
    clf = get_classifier(clf_name)

    data = Data(normalization=predihood.config.NORMALIZATION, filtering=True)
    data.init_all_in_one()
    all_predictions = {}
    message = ""
    for key in neighbourhoods_codes_to_predict:
        neighbourhood_code = neighbourhoods_codes_to_predict[key]
        predictions, current_message = predict_one_neighbourhood(neighbourhood_code, data, clf, 0.8, 0.2, remove_underrepresented=predihood.config.VARIABLE_REMOVE_LOW_REPRESENTATIVITY)  # clf
        all_predictions[neighbourhood_code] = predictions
        if message != current_message: message = current_message
    return {"predictions": all_predictions, "message": message}


@app.route('/get-neighbourhoods-polygon', methods=["GET"])
def get_neighbourhoods_for_polygon():
    """
    Get the list of neighbourhoods in the given polygon. The polygon is defined by two points given in the AJAX request.

    Returns:
        A list of the neighbourhoods that are in the given polygon.
    """
    lat1 = float(request.args['lat1'])
    lng1 = float(request.args['lng1'])
    lat2 = float(request.args['lat2'])
    lng2 = float(request.args['lng2'])
    neighbourhoods = model.get_neighbourhoods_for_polygon(lat1, lng1, lat2, lng2)
    if neighbourhoods is None or len(neighbourhoods) == 0:
        flash("No neighbourhood found in the area.", "warning")
    else:
        flash(str(len(neighbourhoods)) + " neighbourhoods found in the area.", "success")
    return json.dumps({'status': 'OK', 'geojson': neighbourhoods})


@app.route('/count-neighbourhoods-polygon', methods=["GET"])
def count_neighbourhoods_for_polygon():
    """
    Count the number of neighbourhoods in the polygon. The polygon is defined by two points given in the AJAX request.

    Returns:
        The number of neighbourhoods that are in the given polygon.
    """
    lat1 = float(request.args['lat1'])
    lng1 = float(request.args['lng1'])
    lat2 = float(request.args['lat2'])
    lng2 = float(request.args['lng2'])
    nb_iris = model.count_neighbourhoods_for_polygon(lat1, lng1, lat2, lng2)
    return json.dumps({'status': 'OK', 'nbIris': nb_iris})


@app.route('/search-code', methods=["GET"])
def get_neighbourhood_from_code():
    """
    Get a neighbourhood object (represented by a dictionary) given its code.

    Returns:
        An object that represents the neighbourhood corresponding to the given code.
    """
    code_neighbourhood = request.args['code_neighbourhood']
    neighbourhood = model.get_neighbourhood_from_code(code_neighbourhood)
    if neighbourhood is None:
        flash("No corresponding neighbourhood for code " + code_neighbourhood + ".", "warning")
    else:
        flash("Found iris " + code_neighbourhood + ".", "success")
    return json.dumps({'status': 'OK', 'geojson': neighbourhood})


@app.route('/search-name', methods=["GET"])
def get_neighbourhoods_from_name():
    """
    Get neighbourhoods given a query string. The search is performed on neighbourhood's name and neighbourhood's city.

    Returns:
        A list of neighbourhoods corresponding to the given query string.
    """
    query = request.args['querySearch']
    neighbourhoods = model.get_neighbourhoods_from_name(query)
    if neighbourhoods is None or len(neighbourhoods) == 0:
        flash("No corresponding neighbourhoods for query " + query + ".", "warning")
    else:
            flash(str(len(neighbourhoods)) + " neighbourhoods found for query " + query + ".", "success")
    return json.dumps({'status': 'OK', 'geojson': neighbourhoods})


@app.route('/get-variables-values', methods=["GET"])
def get_variables_and_values():
    variables_with_values = {}
    if predihood.config.PREFERRED_LANGUAGE == "fr":
        for env in predihood.config.ENVIRONMENT_VALUES:
            temp = []
            for key in predihood.config.ENVIRONMENT_VALUES[env]:
                temp.append(key)  # get french values
            variables_with_values[env] = temp
    else:
        for env in predihood.config.ENVIRONMENT_VALUES:
            temp = []
            for key in predihood.config.ENVIRONMENT_VALUES[env]:
                temp.append(predihood.config.ENVIRONMENT_VALUES[env][key])  # get english values
            variables_with_values[env] = temp
    return json.dumps(variables_with_values)  # {"result": variables_with_values}


@app.route('/add-neighbourhood-csv', methods=["GET"])
def add_neighbourhood_to_csv():
    """
    Adds an assessed IRIS to a CSV file. Not available in production mode.

    Returns:
        A message that explain the status of the request, i.e. OK if the IRIS has been added, KO else.
    """
    assessed_values = []
    for env in predihood.config.ENVIRONMENT_VALUES:
        assessed_values.append(request.args[env])
    message = add_assessment_to_file(request.args['code_iris'], assessed_values)
    return json.dumps({"status": message})


@app.route("/add-neighbourhoods-file", methods=["GET"])
def add_neighbourhoods_file():
    assessed_iris_file = pd.read_csv(predihood.config.FILE_MANUAL_ASSESSMENT_EXPERTS)
    for index, row in assessed_iris_file.iterrows():
        code_iris = str(row["code"]) if len(str(row["code"])) == 9 else "0"+str(row["code"])
        assessed_values = []
        for env in predihood.config.ENVIRONMENT_VALUES:
            assessed_values.append(row[env])
        add_assessment_to_file(code_iris, assessed_values)
    return json.dumps({"status": "Your IRIS have been added."})


@app.route("/get-preferred-language", methods=["GET"])
def get_preferred_language():
    return json.dumps({"preferred_language": predihood.config.PREFERRED_LANGUAGE})


@app.route('/favicon.ico')
@app.route('/<page>/favicon.ico')
def favicon():
    """
    Display the favicon.

    Returns:
        The favicon.
    """
    return send_from_directory(os.path.join(app.root_path, 'static'), 'favicon.png', mimetype='image/favicon.png')


if __name__ == '__main__':
    dataset_config_file = os.getenv('CONFIG')
    if len(sys.argv) == 2 and os.path.isfile(sys.argv[1]):  # one argument provided (should be the dataset config file)
        log.info(f"You have loaded the dataset from the main's arguments")
        dataset_config_file = sys.argv[1]

    if dataset_config_file is None or not dataset_config_file:
        dataset_config_file = 'datasets/hil/config.json'
        log.warning(f"No parameter provided or incorrect parameter, loading default dataset configuration file ({dataset_config_file})")
    else:
        log.info(f"Loading dataset configuration file: {dataset_config_file}")
    #  check and load the JSON dataset config file
    dataset_config = load_dataset_config(dataset_config_file)
    # set the current dataset used
    predihood.config.CURRENT_CONFIG = dataset_config_file.split("/")[1]
    # if os.path.join("bird-migration", "config.json") in dataset_config_file:
    #     predihood.config.CURRENT_CONFIG = "bird-migration"
    # elif os.path.join("hil", "config.json"):
    #     predihood.config.CURRENT_CONFIG = "hil"
    create_config_dictionaries(dataset_config)
    predihood.config.NORMALIZATION = dataset_config["NORMALIZATION"] if dataset_config["NORMALIZATION"] != "None" else None
    predihood.config.VARIABLE_REMOVE_LOW_REPRESENTATIVITY = dataset_config["VARIABLE_REMOVE_LOW_REPRESENTATIVITY"] if dataset_config["VARIABLE_REMOVE_LOW_REPRESENTATIVITY"] != "None" else None
    if not dataset_config:  # error loading configuration file
        log.error(f"Error loading the dataset config file {dataset_config_file}! Exiting application. ")
        sys.exit(1)
    model.connect(host=host, port=port, dbname=dataset_config['DATABASE_NAME'], coll_neighbourhoods=dataset_config['COLLECTION_NAME'])
    webbrowser.open_new(url)
    app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0  # do not cache files, especially static files such as JS
    app.secret_key = 's3k_5Et#fL45k_#ranD0m-(StuF7)'
    app.run(host=os.getenv('HOST_FLASK'), port=8081)  # debug=True, host option needed inside docker container

