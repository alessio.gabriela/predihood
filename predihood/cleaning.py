"""
Methods for cleaning dataset `hil` and producing some statistical plots. Not directly used by Predihood, but may be useful for new datasets.
"""

# eternal hack for python imports (here to access parent directory)
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

import logging
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from predihood.classes.MethodCleaning import MethodCleaning
from predihood.config import FILE_DATA_HIL, OLD_PREFIX, NEW_PREFIX, FOLDER_DISTRIBUTION

log = logging.getLogger(__name__)


def clean():
    """
    Clean Home in Love data and generate some charts to study distribution of data.
    """
    # 1. read data from Excel file
    data = pd.read_excel(FILE_DATA_HIL)

    # 2. rename dataset's columns because of bad naming convention
    columns = ["id", "name", "HiL_id", "sex", "age", "nb_child", "income", "monthly_charges", "tax_revenue"]
    for elem in ["address", "country", "occupation", "rent"]: columns.append(OLD_PREFIX + elem)
    for elem in ["address", "country", "occupation"]: columns.append(NEW_PREFIX + elem)
    columns.append("reason")
    for elem in ["context", "status", "building_type", "building_usage", "landscape", "morphological_position", "geographical_position", "social_class"]: columns.append(OLD_PREFIX + elem)
    for elem in ["context", "building_type", "building_usage", "landscape", "morphological_position", "geographical_position", "social_class"]: columns.append(NEW_PREFIX + elem)
    data.columns = columns
    data.head()

    # 3. clean data by changing misspelled values
    cleaning = MethodCleaning("cleaning", data)
    cleaning.clean()
    log.info("Many plots have be generated in " + FOLDER_DISTRIBUTION)

    # 4. distribution between women and men
    women_men = data["sex"].value_counts()
    labels = "Women", "Men"
    number_men_women = [women_men["Femme"], women_men["Homme"]]  # getting number of women and number of men
    colors = ["salmon", "lightblue"]
    plt.pie(number_men_women, labels=labels, colors=colors, autopct='%i%%', shadow=True, startangle=90)
    plt.axis("equal")
    plt.title("Distribution of gender")
    plt.show()

    # 5. distribution between ages
    data_temp = data
    data_temp = data_temp.dropna()  # remove NaN values
    ages_plot = []
    total_plot = []
    min_age, max_age = int(min(data_temp["age"])), int(max(data_temp["age"]))
    for counter in range(min_age, max_age + 1):
        total_plot.append(data_temp.loc[data_temp.age == float(counter), "age"].count())
        ages_plot.append(counter)
    mean = np.average(ages_plot, weights=total_plot)

    # First view: bar chart
    plt.bar(ages_plot, total_plot)
    plt.axvline(x=mean, color="red")  # draw median age as a line
    plt.xlabel("Age (in years)")
    plt.ylabel("Number of people")
    plt.title("Distribution of age")
    plt.show()

    # Second view: histogram
    ages = data_temp["age"]
    plt.hist(ages, facecolor="gray", align="mid")
    plt.xlabel("Age (in years)")
    plt.ylabel("Number of people")
    plt.title("Distribution of age")
    plt.show()

    # 6. distribution between incomes
    incomes = data["income"]
    plt.hist(incomes, facecolor="gray", align="mid")
    plt.title("Distribution  of monthly income")
    plt.xlabel("Monthly income (in euros)")
    plt.ylabel("Number of people")
    plt.show()

    # 7. distribution between reasons of transfer
    transfers = data["reason"].value_counts()
    labels = transfers.index.tolist()
    sizes = [transfers[i] for i in range(len(transfers))]
    plt.pie(sizes, labels=labels, autopct="%.2f", shadow=True, startangle=90)
    plt.axis("equal")
    plt.title("Distribution of the reason of job transfer")
    plt.show()

    # 8. distribution between geographic positions
    geo = pd.concat([data[OLD_PREFIX + "geographical_position"], data[NEW_PREFIX + "geographical_position"]], ignore_index=True)
    split_geo = [geo[i].split()[0] if not isinstance(geo[i], float) else "" for i in range(len(geo))]
    set_geo = set(split_geo)
    uniques = [split_geo.count(elem) for elem in set_geo]
    labels = set_geo
    plt.pie(uniques, labels=labels, autopct="%.2f", shadow=True, startangle=90)
    plt.title("Distribution of the geographical position")
    plt.show()

    # 9. draw evolutions before and after job transfer for each EV
    cleaning.to_chart("occupation", "status", "Evolution of status before and after job transfer")
    cleaning.to_chart("building_type", "building_type", "Evolution of building type before and after job transfer")
    cleaning.to_chart("building_usage", "building_usage", "Evolution of building usage before and after job transfer")
    cleaning.to_chart("landscape", "landscapes", "Evolution of landscapes before and after job transfer")
    cleaning.to_chart("social_class", "social", "Evolution of social classes before and after job transfer")
    cleaning.to_chart("morphological_position", "morpho", "Evolution of morphological positions before and after job transfer")
    cleaning.to_chart("geographical_position", "geo", "Evolution of geographical positions before and after job transfer")


if __name__ == '__main__':
    clean()
