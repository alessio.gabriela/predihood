"""
Methods for generating lists of the most relevant indicators (10, 20, 30, 40, 50, 75, 100, and all, see `config.TOPS_K`) later used for prediction.
"""

import json
import logging
import numpy as np
import os
import pandas as pd

from predihood.classes.Data import Data
from predihood.classes.Dataset import Dataset
from predihood.classes.MethodSelection import MethodSelection
import predihood.config
from predihood.config import TOPS_K, FOLDER_SELECTED_INDICATORS, FILE_HIERARCHY, CURRENT_CONFIG, FOLDER_DATASETS
from predihood.utility_functions import apply_hierarchy
from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier

log = logging.getLogger(__name__)


def generate_all_data():
    """
    Generate all datasets, i.e. one with density normalization, one for population normalization and one without normalization.
    The three are filtered. Generated datasets are located in generated_files/datasets.
    """
    data = Data(normalization="density", filtering=True)
    data.init_all_in_one()
    data = Data(normalization="population", filtering=True)
    data.init_all_in_one()
    data = Data(normalization=None, filtering=True)
    data.init_all_in_one()


def retrieve_lists(add_assessment=False, top_k=None):
    """
    Get all lists generated for each top-k or for the given top-k if provided.

    Args:
        top_k: a integer corresponding to the top-k of the list to retrieve

    Returns:
        a dictionary containing the lists of indicators for each top-k
    """
    lists = {}

    if top_k:
        # a top-k is specified, so the list of indicators of size top-k is retrieved
        lst = retrieve_one_list(add_assessment, top_k)
        lists[str(top_k)] = lst
    else:
        # no top-k is provided, so all lists form 10 to 100 indicators are retrieved
        for top_k in TOPS_K:
            lst = retrieve_one_list(add_assessment, top_k)
            lists[str(top_k)] = lst
    return lists


def retrieve_one_list(add_assessment, top_k):
    """
    Retrieve the list of selected INSEE indicators corresponding to the given top-k.

    Args:
        top_k: an integer corresponding to the size of the list to retrieve

    Returns:
        a list containing indicators of the list of size top-k
    """
    if not os.path.exists(os.path.join(FOLDER_DATASETS, predihood.config.CURRENT_CONFIG, "list" + str(top_k) + ".csv")): generate_lists(add_assessment)
    indicators_csv = pd.read_csv(os.path.join(FOLDER_DATASETS, predihood.config.CURRENT_CONFIG, "list" + str(top_k) + ".csv"), header=None)
    lst = json.loads(indicators_csv.drop(indicators_csv.columns[0], axis=1).to_json(orient="index"))
    list_temp = {}
    for key, value in lst.items():
        list_temp[predihood.config.ENVIRONMENT_VARIABLES[int(key)]] = [value2 for key2, value2 in value.items() if value2 is not None]
    return list_temp


def generate_lists(add_assessment=False):
    """
    Generates lists of INSEE indicators that are relevant for prediction.
    This selection process is based on:
    - removing fully correlated indicators
    - select a limited number among the most relevant indicators (using Random Forest and Extra Tree classifiers)
    - taking into account the diversity of categories of INSEE indicators based on a hierarchy of these indicators
    """
    # 1. Create data
    data = Data(normalization=predihood.config.NORMALIZATION, filtering=True, add_assessment=add_assessment)
    data.init_all_in_one()

    # 2. Run heat map and get fully correlated indicators by using a correlation matrix
    dataset = Dataset(data, "building_type", "unsupervised")
    dataset.init_all_in_one()
    heat_map = MethodSelection(name="heat map", dataset=dataset, parameters={"method": "spearman"})
    heat_map.compute_selection()
    fully_correlated_indicators = heat_map.best_indicators
    log.info("fully correlated indicators: %d %s", len(fully_correlated_indicators), ", ".join(fully_correlated_indicators))

    # 3. Select a limited number (top-k) of indicators by using Random Forest and Extra Tree classifiers.
    # Then take into account the diversity of indicators by using a hierarchy of the INSEE indicators
    hierarchy = pd.read_csv(FILE_HIERARCHY, sep="\t")

    # 4. For each size of list (i.e. for each top-k), select a limited number of indicators with Extra Tree and Random Forest classifiers
    # Then merge the two results to obtain a single list of relevant indicators.
    for top_k in TOPS_K:
        log.info("constructing list of %d indicators", top_k)
        all_lists = []  # to keep lists of indicators for each EV (for the current top-k)

        for env in predihood.config.ENVIRONMENT_VARIABLES:
            dataset = Dataset(data, env, indicators_to_remove=fully_correlated_indicators, _type="supervised")
            dataset.init_all_in_one()

            # a. get best indicators according to Extra Tree classifier
            fi_et = MethodSelection(name="feature importance ET", dataset=dataset, classifier=ExtraTreesClassifier(), parameters={"top_k": top_k})
            fi_et.fit()
            fi_et.compute_selection()
            best_indicators_ET = fi_et.best_indicators

            # b. get best indicators according to Random Forest classifier
            fi_rf = MethodSelection(name="feature importance RF", dataset=dataset, classifier=RandomForestClassifier(), parameters={"top_k": top_k})
            fi_rf.fit()
            fi_rf.compute_selection()
            best_indicators_RF = fi_rf.best_indicators

            # c. merge indicators that have been selected by ET and RF classifiers
            # in this step, if an indicator have been selected by the two classifiers, its score is the addition of its score for RF and the one for ET.
            best_indicators_ET.extend(best_indicators_RF)
            all_selected_indicators = best_indicators_ET  # all selected indicators, i.e. union between RF and ET
            keys = set([element[0] for element in all_selected_indicators])  # store indicators' names selected
            merged_indicators_temp = {key: 0 for key in keys}

            for i in range(len(all_selected_indicators)):
                indicator = all_selected_indicators[i]
                merged_indicators_temp[indicator[0]] += indicator[1]  # adding score

            # transform it into a list of sub-lists, e.g. [[indicator1, score1], ..., indicatorN, scoreN]]
            merged_indicators = [[key, merged_indicators_temp[key]] for key in merged_indicators_temp]

            # d. apply hierarchy on selected indicators to taking into account the diversity of categories of indicators
            indicators_hierarchy = apply_hierarchy(merged_indicators, hierarchy)

            # get the names of each selected indicator
            selected_indicators_names = [indicator[0] for indicator in indicators_hierarchy]

            # e. add uncorrelated indicators of heat map to the lists
            all_lists.append(selected_indicators_names)

        # C. Transform lists of selected indicators for the current top-k and save it as a CSV file
        # indexes = {i: ENVIRONMENT_VARIABLES[i] for i in range(len(ENVIRONMENT_VARIABLES))}
        selected_indicators = pd.DataFrame(np.array(all_lists).tolist())
        selected_indicators.to_csv(os.path.join(FOLDER_DATASETS, predihood.config.CURRENT_CONFIG, "list"+str(top_k)+".csv"), header=False)


if __name__ == "__main__":
    generate_all_data()
    generate_lists()
