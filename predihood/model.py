#!/usr/bin/env python
# encoding: utf-8
"""
Methods for retrieving data stored in MongoDB using the [mongiris API](https://gitlab.liris.cnrs.fr/fduchate/mongiris) and optionally transforming it.
"""

import json
import os
import re
from mongiris.api import Mongiris

# global connection variables
db = None
collection = None
json_iris_indicator_code_to_label = {}  # 'static/data/dictionnaire-indicateurs.json'


def connect(host, port, dbname, coll_neighbourhoods, coll_indicators='collindic'):
    """
    Connect to the MongoDB database.
    Parameters
    ----------
    dbname the name of the DB to connect (i.e. dbinsee or bird-migration).
    coll_neighbourhoods the name of the collection that contains neighbourhoods (often colliris).
    coll_indicators the name of the collection that contains indicators (often collindic).

    Returns
    -------

    """
    global db, collection
    db = Mongiris(host, port, dbname, coll_neighbourhoods, coll_indicators)
    collection = db.collection_neighbourhoods

    # construct the json of indicators from the DB
    for indicator in db.collection_indicators.find():
        json_iris_indicator_code_to_label[indicator["short_label"]] = {
            # "insee_files": indicator["from_insee_files"],
            "long_fieldname": indicator["full_label"]
        }


def get_neighbourhoods_for_polygon(lat1, lng1, lat2, lng2):
    """
    Get neighbourhoods that are in a box represented by given coordinates.

    Args:
        lat1: a float for latitude of the first point of the box
        lng1: a float for longitude of the first point of the box
        lat2: a float for latitude of the second point of the box
        lng2: a float for longitude of the second point of the box

    Returns:
        a list of neighbourhoods that are in the box defined by given latitudes and longitudes
    """
    # polygon = db.convert_geojson_box_to_polygon(lng1, lat1, lng2, lat2)
    polygon = Mongiris.convert_geojson_box_to_polygon(lng1, lat1, lng2, lat2)
    # neighbourhood = db.geo_within(neighbourhood_collection, polygon)
    neighbourhood = db.intersect(collection, polygon)
    return neighbourhood


def count_neighbourhoods_for_polygon(lat1, lng1, lat2, lng2):
    """
    Count neighbourhoods that are in a box represented by given coordinates.

    Args:
        lat1: a float for latitude of the first point of the box
        lng1: a float for longitude of the first point of the box
        lat2: a float for latitude of the second point of the box
        lng2: a float for longitude of the second point of the box

    Returns:
        an integer representing the number of neighbourhoods in the box defined by given latitudes and longitudes
    """
    polygon = db.convert_geojson_box_to_polygon(lng1, lat1, lng2, lat2)
    neighbourhoods = db.geo_within(collection, polygon)
    if neighbourhoods is None: return 0
    return len(neighbourhoods)


def get_neighbourhood_from_code(code):
    """
    Get a neighbourhood given its code.

    Args:
        code: a string corresponding to the code of the neighbourhood

    Returns:
        an object that represents the neighbourhood corresponding to the given code
    """
    neighbourhood = db.get_neighbourhood_from_code(code)
    return neighbourhood


def get_neighbourhoods_from_name(name):
    """
    Get neighbourhoods given a name (searched both in neighbourhood name and city).

    Args:
        name: a string corresponding to the name of the neighbourhood or the name of its city.

    Returns:
        an object that represents the neighbourhoods corresponding to the given name
    """
    # the query string (name) is searched in both the name of the neighbourhood and the name of the city
    regx = re.compile(name, re.IGNORECASE)
    query_clause = {"$or": [{"properties.NOM_IRIS": {"$regex": regx}}, {"properties.NOM_COM": {"$regex": regx}}]}
    neighbourhoods = db.find_documents(collection, query_clause)
    return neighbourhoods


def parse_json_to_dict(json_file_path):
    """
    Convert a JSON file to a dictionary object.

    Args:
        json_file_path: a string containing the path of the file to load

    Returns:
        a dictionary containing the data in the file located in the given path
    """
    assert(os.path.isfile(json_file_path))
    with open(json_file_path, encoding="utf-8") as data_file:
        data = json.load(data_file)
        data_file.close()
        return data


def get_coords_from_code(code):
    """
    Get geometry of the neighbourhood corresponding to the given code.

    Args:
        code: a string corresponding to the code of the neighbourhood

    Returns:
        a list representing coordinates of the neighbourhood
    """
    neighbourhood = db.get_neighbourhood_from_code(code)
    if neighbourhood:
        return db.get_geojson_polygon(neighbourhood["geometry"]["coordinates"])
    else:
        return None


def get_indicators_list():
    """
    Get all INSEE indicators that are stored in the database. This corresponds to the collection 'collindic'.

    Returns:
        a list containing all names of indicators, e.g. ['POP0002', 'POP0204', ..., 'P14_RP_MAISON']
    """
    list_indicators = db.find_all(db.collection_indicators)
    return [indicator["short_label"] for indicator in list_indicators]


def get_indicators_dict():
    """
    Get all INSEE indicators that are stored in the database. This corresponds to the collection 'collindic'.

    Returns:
        a dictionary containing all names of indicators, e.g. {'POP0002': 'Population aged from 0 to 2 y.o.', ..., 'P14_RP_MAISON': 'Number of principal residences'}
    """
    list_indicators = db.find_all(db.collection_indicators)
    return {indicator["short_label"]: indicator["full_label"] for indicator in list_indicators}
