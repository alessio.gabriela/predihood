# eternal hack for python imports (here to access parent directory)
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..'))

import logging
import math

import numpy as np
import pandas as pd
import warnings
from area import area
import predihood.config

from predihood import model
from predihood.cleaning import clean
from predihood.config import FILE_CLEANED_DATA, FOLDER_DATASETS, FILE_GROUPING, NEW_PREFIX, OLD_PREFIX, FILE_MANUAL_ASSESSMENT_EXPERTS
from predihood.utility_functions import address_to_code, append_indicator, append_target

log = logging.getLogger(__name__)
warnings.filterwarnings("ignore", category=RuntimeWarning)


class Data:
    def __init__(self, normalization="density", filtering=True, add_assessment=False):
        """
        Constructor of the Data class. Initialize attributes.
        Args:
            normalization: A string to indicate the choice for normalization ("density" for density, "population" for population and None to do not normalize)
            filtering: True or False to indicate if useless indicators will be removed or not
        """
        self.old_path = FILE_CLEANED_DATA
        self.dataset_path = os.path.join(FOLDER_DATASETS, predihood.config.CURRENT_CONFIG, "data_"+str(normalization)+".csv")
        self.filtered_path = os.path.join(FOLDER_DATASETS, predihood.config.CURRENT_CONFIG, "data_"+str(normalization)+"_filtered.csv")
        self.assessment_path = os.path.join(FOLDER_DATASETS, predihood.config.CURRENT_CONFIG, "data_" + str(normalization) + "with_assessment.csv")
        self.assessment_filtered_path = os.path.join(FOLDER_DATASETS, predihood.config.CURRENT_CONFIG, "data_" + str(normalization) + "_filtered_with_assessment.csv")
        self.data = None
        self.indicators = None
        self.normalization = normalization
        self.filtering = filtering
        self.add_assessment = add_assessment

        # retrieve indicators
        self.get_indicators()
        log.debug("Starting with %d indicators", len(self.indicators))
        # define indicators that are not relevant for the prediction and remove them
        indicators_to_remove = ["IRIS", "REG", "DEP", "UU2010", "COM", "LIBCOM", "TRIRIS", "GRD_QUART", "LIBIRIS", "TYP_IRIS", "MODIF_IRIS", "LAB_IRIS", "LIB_IRIS", "LIB_COM", "CODGEO", "LIBGEO"]
        for indicator in indicators_to_remove:
            if indicator in self.indicators:
                self.indicators.remove(indicator)
        log.debug("Descriptive indicators: %d. It remains %d indicators", len(indicators_to_remove), len(self.indicators))

    def get_indicators(self):
        """
        Get indicators from the dataset if it exists, else from the database.
        """
        if self.data is not None:
            self.indicators = self.data.columns.tolist()
            # self.data.columns gets all columns, so to get indicators, we remove "CODE" (since it is not relevant for prediction).
            # We keep "AREA" and "DENSITY" since they are used as features in prediction
            if "CODE" in self.indicators: self.indicators.remove("CODE")
            for env in predihood.config.ENVIRONMENT_VARIABLES:
                if env in self.indicators:
                    self.indicators.remove(env)
        else:
            self.indicators = model.get_indicators_list()

    def init_all_in_one(self):
        """
        Create or read the dataset. It applies normalization and/or filtering if needed.
        """
        if self.add_assessment:
            if not os.path.exists(self.assessment_path):
                if os.path.exists(self.dataset_path):
                    self.data = pd.read_csv(self.dataset_path)  # data is already created so just read it
                else:
                    self.create()  # data does not exist, need to create it
                self.add_manually_assessed_iris()  # complete data with manually assessed IRIS
                self.apply_normalization()
                if self.filtering: self.apply_filtering()
            elif not os.path.exists(self.assessment_filtered_path):
                if self.filtering: self.apply_filtering()
            else:
                self.read()
        else:
            if not os.path.exists(self.dataset_path):
                self.create()
                self.apply_normalization()
                if self.filtering: self.apply_filtering()
            elif not os.path.exists(self.filtered_path):
                if self.filtering: self.apply_filtering()
            else:
                self.read()

    def read(self):
        """
        Read the dataset stored in the CSV file (and get indicators from this dataset).
        """
        if self.add_assessment:
            if self.filtering:
                self.data = pd.read_csv(self.assessment_filtered_path)
            else:
                self.data = pd.read_csv(self.assessment_path)
        else:
            if self.filtering:
                self.data = pd.read_csv(self.filtered_path)
            else:
                self.data = pd.read_csv(self.dataset_path)
        self.get_indicators()

    def create(self):
        """
        Construct the dataset from HomeInLove data, i.e. ungroup addresses, retrieve information about IRIS and construct a CSV file.
        """
        log.info("... construction of dataset is in progress ...")

        raw_data = []
        null_iris = 0  # count IRIS that can't be retrieve from its code
        problems = 0  # count IRIS that encounter problems and that will be not added to the dataset
        cols = []
        cols.append("CODE")
        cols.append("AREA")
        append_col = True

        if predihood.config.CURRENT_CONFIG == "hil":
            # 1. read the data to be transformed
            if not os.path.exists(self.old_path):
                clean()
            data = pd.read_csv(self.old_path)

            # 2. define some variables
            foreigners = 0  # count addresses that are not located in France
            columns = ["address", "country"]
            columns.extend(predihood.config.ENVIRONMENT_VARIABLES)
            cols.append("DENSITY")
            cols_departure = [OLD_PREFIX+env for env in columns]
            cols_arrival = [NEW_PREFIX+env for env in columns]

            # 3. get data for departure and arrival IRIS
            departures = data[cols_departure]
            arrivals = data[cols_arrival]
            # remove "old_address", "old_country", "new_address", "new_country"
            cols_departure.pop(0)
            cols_departure.pop(0)
            cols_arrival.pop(0)
            cols_arrival.pop(0)

            # 4. for each departure and arrival neighbourhood (one line in cleanedData.csv), get its indicators and its EV.
            # for each neighbourhood store its code, compute and store its area and its density, store all its indicators and its EV.
            for (index1, row1), (index2, row2) in zip(departures.iterrows(), arrivals.iterrows()):
                dep_row, arr_row = [], []
                # convert address to neighbourhood code and append it to data only if the address is in France
                if row1[OLD_PREFIX + "country"] == "France":
                    dep_code = address_to_code(row1[OLD_PREFIX + "address"])
                    dep_row.append(dep_code)
                    dep_iris = model.get_neighbourhood_from_code(dep_code)  # get IRIS information
                    coords_dep = model.get_coords_from_code(dep_code)
                    area_dep = area(coords_dep) / 1000000 if coords_dep is not None else None  # convert area from m^2 to km^2
                    dep_row.append(area_dep)  # add area as a feature
                    density = dep_iris["properties"]["raw_indicators"]["P14_POP"] / area_dep if area_dep is not None and area_dep > 0 else None
                    dep_row.append(density)  # add density as a feature

                    if dep_iris is not None:
                        # a. append indicators
                        for raw_indicator in self.indicators:
                            dep_row, cols = append_indicator(raw_indicator, dep_iris, dep_row, append_col, cols)

                        # b. append EV
                        for target in cols_departure:
                            dep_row = append_target(row1, target, dep_row)
                        append_col = False
                        if len(dep_row) > 0: raw_data.append(dep_row)
                        else: problems += 1
                    else: null_iris += 1
                elif row1[OLD_PREFIX+"country"] == "" or row1[OLD_PREFIX+"country"] is None or row1[OLD_PREFIX+"country"] is np.nan: null_iris += 1
                else: foreigners += 1
                if row2[NEW_PREFIX + "country"] == "France":
                    arr_code = address_to_code(row2[NEW_PREFIX + "address"])
                    arr_row.append(arr_code)
                    arr_iris = model.get_neighbourhood_from_code(arr_code)
                    coords_arr = model.get_coords_from_code(arr_code)
                    area_arr = area(coords_arr) / 1000000 if coords_arr is not None else None  # convert area from m^2 to km^2
                    arr_row.append(area_arr)  # add area as a feature
                    density = arr_iris["properties"]["raw_indicators"]["P14_POP"] / area_arr if area_arr is not None and area_arr > 0 else None
                    arr_row.append(density)  # add density as a feature

                    if arr_iris is not None:
                        # a. append INSEE indicators
                        for raw_indicator in self.indicators:
                            arr_row, cols = append_indicator(raw_indicator, arr_iris, arr_row, append_col, cols)

                        # b. append targets
                        for target in cols_arrival:
                            arr_row = append_target(row2, target, arr_row)
                        append_col = False
                        if len(arr_row) > 0: raw_data.append(arr_row)
                        else: problems += 1
                    else: null_iris += 1
                elif row2[NEW_PREFIX + "country"] == "" or row2[NEW_PREFIX + "country"] is None or row2[NEW_PREFIX + "country"] is np.nan: null_iris += 1
                else: foreigners += 1

                sys.stdout.write("\r%.2f%%" % ((index1 * 100) / len(departures)))  # update progress percentage
                sys.stdout.flush()
            print()

            cols.extend(predihood.config.ENVIRONMENT_VARIABLES)
            log.info("%d addresses are not located in France.", foreigners)
            log.info("%d null neighbourhoods have been removed from the dataset.", null_iris)
            log.info("%d neighbourhoods have encountered problems.", problems)
        else:
            # e.g. for bird-migration
            data = pd.read_csv(os.path.join("datasets", predihood.config.CURRENT_CONFIG, "expertise.csv"), delimiter=";")
            target_cols = predihood.config.ENVIRONMENT_VARIABLES  # TODO WARNING
            for index, row in data.iterrows():
                complete_row = []
                # convert address to IRIS code and append it to data only if the address is in France
                code = row.iloc[0]
                neighbourhood_code = str(int(code)) if code is not np.nan else None  # get the first column, i.e. the code of the neighbourhood
                neighbourhood = model.get_neighbourhood_from_code(neighbourhood_code)  # get IRIS information
                coordinates = model.get_coords_from_code(neighbourhood_code)
                complete_row.append(neighbourhood_code)
                area_dep = area(coordinates) / 1000000 if coordinates is not None else None  # convert area from m^2 to km^2
                complete_row.append(area_dep)  # add area as a feature
                # density = neighbourhood["properties"]["raw_indicators"]["P14_POP"] / area_dep if area_dep is not None and area_dep > 0 else None
                # complete_row.append(density)  # add density as a feature

                if complete_row is not None:
                    # a. append indicators
                    for raw_indicator in self.indicators:
                        complete_row, cols = append_indicator(raw_indicator, neighbourhood, complete_row, append_col, cols)

                    # b. append EV
                    for target in target_cols:
                        complete_row = append_target(row, target, complete_row)
                    append_col = False
                    if len(complete_row) > 0:
                        raw_data.append(complete_row)
                    else:
                        problems += 1
                else:
                    null_iris += 1

                sys.stdout.write("\r%.2f%%" % ((index * 100) / len(data)))  # update progress percentage
                sys.stdout.flush()
            print()

            cols.extend(target_cols)
            log.info("%d null neighbourhoods have been removed from the dataset.", null_iris)
            log.info("%d neighbourhoods have encountered problems.", problems)

        # 5. convert IRIS data into a DataFrame, fill missing values and remove fully empty columns.
        self.data = pd.DataFrame(raw_data, columns=cols)
        self.data.sort_values("CODE", inplace=True)
        self.fill_missing_values("median")
        nan_columns = self.data.columns[self.data.isna().all()].tolist()
        log.debug("There are %d NaN columns", len(nan_columns))
        self.data.drop(nan_columns, axis=1, inplace=True)
        self.indicators = [indicator for indicator in self.indicators if indicator not in nan_columns]  # remove names of NaN columns in the list of indicators

    def fill_missing_values(self, method="median"):
        """
        Fill NaN values given the method.
        Args:
            method: A string corresponding tto the method for filling NaN values ("zero", "mean" or "median"). Default is median.
        """
        assert method in ["zero", "mean", "median"]
        cols = ["CODE", "AREA", "DENSITY"]
        cols.extend(predihood.config.ENVIRONMENT_VARIABLES)  # TODO WARNING
        for col in self.data.iteritems():
            value = 0
            col_name = col[0]
            if col_name not in cols:
                # fill missing INSEE indicators
                if method == "zero":
                    value = 0
                elif method == "mean":
                    value = self.data[col_name].mean()
                elif method == "median":
                    value = self.data[col_name].median()
                else:
                    value = np.mean(self.data[col_name])
            elif col_name not in ["CODE", "AREA", "DENSITY"]:
                # fill missing EV
                local_config_dict = predihood.config.CURRENT_CONFIG_DICT
                if method == "zero":
                    env_values = { var_name: value["low_influence_value"] for var_name, value in local_config_dict["VARIABLES_VALUES"][predihood.config.PREFERRED_LANGUAGE].items() }
                else:  # method == "mean" or method == "median"
                    env_values = { var_name: value["median_value"] for var_name, value in local_config_dict["VARIABLES_VALUES"][predihood.config.PREFERRED_LANGUAGE].items() }
                value = env_values[col_name]
            # else: -> column is CODE or AREA or DENSITY -> do nothing
            self.data[col_name].replace([np.nan], [value], inplace=True)

    def filter_too_detailed_indicators(self):
        """
        Remove too detailed indicators from the dataset. The list of indicators is given by the file `regrouping.csv`.
        """
        regrouping = pd.read_csv(FILE_GROUPING, sep="\t")
        status = regrouping.index[regrouping["STATUS"] == 1].tolist()  # indicators that are an element of a subset
        col_names = [regrouping.iloc[status]["INDICATOR"] for status in status]
        # self.filtered_data = self.data[:] # copy by value, not by reference
        counter1, counter2 = 0, 0
        for column in col_names:
            if column in self.data and column in self.indicators and column:
                del self.data[column]
                self.indicators.remove(column)
                counter1 += 1
        log.debug("%d indicators have been using regrouping.", counter1)

    def apply_filtering(self):
        """
        Remove (filter) too detailed indicators and constant columns from the dataset.
        """
        if self.data is None:
            if self.add_assessment:
                self.data = pd.read_csv(self.assessment_path)
            else:
                self.data = pd.read_csv(self.dataset_path)
            self.get_indicators()

        # 1. remove indicators that have been defined as useless
        self.filter_too_detailed_indicators()

        # 2. remove constant columns, i.e. with a null variance
        constant_columns = self.data.columns[self.data.nunique() == 1].tolist()
        self.data.drop(constant_columns, axis=1, inplace=True)
        self.indicators = [indicator for indicator in self.indicators if indicator not in constant_columns]

        if self.add_assessment: self.data.to_csv(self.assessment_filtered_path, index=None)
        else: self.data.to_csv(self.filtered_path, index=None)  # index=None: avoid line numbering

    def apply_normalization(self):
        """
        Normalize the dataset with the given method, i.e. None, "population" or "density". Default is "density".
        """
        assert self.normalization in [None, "population", "density"]
        do_not_normalize = ["CODE", "AREA", "DENSITY", "P14_POP"]
        do_not_normalize.extend(predihood.config.ENVIRONMENT_VARIABLES)  # extend does not return a list
        for index, row in self.data.iterrows():
            for column in row.iteritems():
                col_name = column[0]
                if col_name not in do_not_normalize:
                    if self.normalization == "population":
                        self.data.at[index, col_name] = row[col_name] / row["P14_POP"]
                    elif self.normalization == "density":
                        density = row[self.data.columns.get_loc("DENSITY")]
                        self.data.at[index, col_name] = row[col_name] / density if density > 0 else 0
                    elif self.normalization is None:
                        self.data.at[index, col_name] = row[col_name]
        if self.add_assessment: self.data.to_csv(self.assessment_path, index=None)
        else: self.data.to_csv(self.dataset_path, index=None)  # index=None: avoid line numbering

    def add_manually_assessed_iris(self):
        manually_assessed_iris = pd.read_csv(FILE_MANUAL_ASSESSMENT_EXPERTS, header=0)
        self.data = self.data.append(manually_assessed_iris)
        self.data["CODE"] = self.data["CODE"].astype(str)
        self.data.sort_values("CODE", inplace=True)
        self.fill_missing_values("median")
        nan_columns = self.data.columns[self.data.isna().all()].tolist()
        log.debug("There are %d NaN columns", len(nan_columns))
        self.data.drop(nan_columns, axis=1, inplace=True)
        self.indicators = [indicator for indicator in self.indicators if indicator not in nan_columns]  # remove names of NaN columns in the list of indicators


