# eternal hack for python imports (here to access parent directory)
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..'))

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

from predihood.classes.Method import Method
from predihood.config import RANDOM_STATE, TITLES


class MethodSelection(Method):
    """
    This class represents a method for selection a subset of indicators among all INSEE indicators.
    """
    def __init__(self, name, dataset, classifier=None, transform=False, parameters=None):
        """
        Constructor of the MethodSelection class. Initialize attributes.
        """
        Method.__init__(self, name, dataset, classifier)
        self.transform = transform
        self.threshold = None
        self.best_indicators = None
        self.parameters = parameters

    def fit(self):
        """
        Fit the train data on the classifier.
        """
        if self.classifier is not None:
            self.classifier.random_state = RANDOM_STATE
            if self.transform:
                self.classifier.fit_transform(self.dataset.X_train, self.dataset.Y_train)
            else:
                self.classifier.fit(self.dataset.X_train, self.dataset.Y_train)

    def compute_selection(self):
        """
        Get results of the classifier according to its name.
        """
        if self.name == "feature importance ET" or self.name == "feature importance RF":
            importance = self.classifier.feature_importances_
            indicators_importance = {self.dataset.indicators[i]: importance[i] for i in range(len(importance))}  # create a dictionary to associate each indicator with its importance
            indicators_importance = {k: v for k, v in sorted(indicators_importance.items(), key=lambda item: item[1], reverse=True)}  # order dictionary by value in descending order
            k_best = [[key, value] for key, value in indicators_importance.items()][:self.parameters["top_k"]]
            self.best_indicators = k_best
        elif self.name == "heat map":
            fig, ax = plt.subplots()
            ax.xaxis.tick_top()

            # get indicators that are fully correlated (i.e. corr=1)
            temp_data = self.dataset.data[self.dataset.indicators][:]  # get only INSEE indicators (!= CODE, AREA, EV)
            corr_matrix = temp_data.corr(method=self.parameters["method"]).abs()
            sns.heatmap(corr_matrix)
            upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape), k=1).astype(np.bool))

            self.best_indicators = []
            for i in range(len(upper.columns)):
                column = upper.columns[i]
                for k, value in upper[column].items():
                    if value == 1 and column not in self.best_indicators: # and (column, k) not in self.best_indicators and (k, column) not in self.best_indicators:
                        self.best_indicators.append(column)

            if TITLES: plt.title("Correlation matrix: filtering = " + self.dataset.filtering + ", normalization = " + self.dataset.normalization)
            plt.show()
        else:
            raise Exception("Unknown name. Choose among [\"feature importance ET\", \"feature importance RF\", \"heat map\"].")
