# eternal hack for python imports (here to access parent directory)
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..'))

import logging
#import os
import warnings
import logging
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from predihood.classes.Method import Method
from predihood.config import FILE_CLEANED_DATA, FOLDER_DISTRIBUTION, OLD_PREFIX, NEW_PREFIX
from predihood.utility_functions import similarity, auto_label

log = logging.getLogger(__name__)
warnings.simplefilter(action='ignore', category=FutureWarning)


class MethodCleaning(Method):
    """
    This class represents the method for cleaning data given by the French company.
    """
    def __init__(self, name, dataset):
        """
        Constructor of the MethodCleansing class. Initialize attributes.
        """
        Method.__init__(self, name, dataset)
        self.values_by_env = {}  # dictionary to store values for each EV, e.g. [House, Houses]
        self.columns_dep = []  # departure columns, e.g. old_building_type, old_building_usage,...
        self.columns_arr = []  # arrival columns, e.g. new_building_type, new_building_usage, ...
        self.columns = {
            "occupation": {
                "name_dep": "old_occupation",
                "data_dep": self.dataset.old_occupation,
                "name_arr": "new_occupation",
                "data_arr": self.dataset.new_occupation
            }
        }

        for env in predihood.config.ENVIRONMENT_VARIABLES:
            temp = {
                "name_dep": OLD_PREFIX + env,
                "name_arr": NEW_PREFIX + env,
                "data_dep": self.dataset[OLD_PREFIX + str(env)],
                "data_arr": self.dataset[NEW_PREFIX + str(env)]
            }
            self.columns[env] = temp

        for env in self.columns: self.columns_dep.append(self.columns[env]["data_dep"])
        for env in self.columns: self.columns_arr.append(self.columns[env]["data_arr"])

        # define outliers (to be removed)
        self.outliers = ['Oui', 'Moyenne-sup', 'Location']

        # plot variables
        self.before = {}  # departure values for each EV
        self.after = {}  # arrival values for each EV
        self.labels = {}  # labels for EV, e.g. urban, green areas, forest and country-side for the landscape variable

    def clean(self):
        """
        Clean data from bad naming conventions. The idea of this function is to create a dictionary with all spellings for each value of each EV, e.g. [["Houses", "House"], ["Upper middle", "upper middle", "upper midddle"], ["Green areas"], ["Countryside"]].
        This dictionary is constructed by computing similarities between each values and store each spelling and finally let the user choose the best one.
        """
        #
        log.info("The data needs to be cleaned. For each list, write the correct word. For each EV, you will get its number of corrections and its error rate.")
        # 1. getting wrong values in a dictionary ordered by env variable
        self.values_by_env = {}
        for col_dep, col_arr in zip(self.columns_dep, self.columns_arr):
            col_name = col_dep.name[len(OLD_PREFIX):]
            self.values_by_env[col_name] = []
            for val in col_dep.unique():  # get possible values for the current column
                index = similarity(val, self.values_by_env[col_name])
                # if the value is similar to another, add it, else create an new array with it
                if index >= 0:
                    self.values_by_env[col_name][index].append(val)
                elif index == -1:
                    self.values_by_env[col_name].append([val])
            for val in col_arr.unique():
                index = similarity(val, self.values_by_env[col_name])
                if index >= 0:
                    self.values_by_env[col_name][index].append(val)
                elif index == -1:
                    self.values_by_env[col_name].append([val])

        # 2. renaming these wrong values in data
        for key, value in self.values_by_env.items():
            col_name_old = OLD_PREFIX + key
            col_name_new = NEW_PREFIX + key
            nb_replacement_dep = 0
            nb_replacement_arr = 0
            for i in range(len(value)):
                if len(value[i]) > 1:
                    arr_without_duplicates = list(dict.fromkeys(value[i]))
                    chosen_label = input(str(arr_without_duplicates) + ": ")
                    for label in value[i]:
                        if label != chosen_label:  # if label == chosen_label: skip it because no replacement is needed
                            nb_replacement_dep += pd.Series(self.dataset[col_name_old] == label).sum()
                            nb_replacement_arr += pd.Series(self.dataset[col_name_new] == label).sum()
                            self.dataset.loc[self.dataset[col_name_old] == label, col_name_old] = chosen_label
                            self.dataset.loc[self.dataset[col_name_new] == label, col_name_new] = chosen_label
            size = int(self.dataset.count()[OLD_PREFIX + key]) + int(self.dataset.count()[NEW_PREFIX + key])
            mean_error = ((nb_replacement_dep + nb_replacement_arr) / size) * 100
            log.debug("%d IRIS have been corrected for the EV %s, corresponding to an error rate of %.0f %%", (nb_replacement_dep + nb_replacement_arr), key, mean_error)

        # 3. removing outliers from data
        count = 0
        for outlier in self.outliers:
            self.dataset.drop(self.dataset[self.dataset.eq(outlier).any(1)].index, inplace=True)
            count += 1
        log.debug("%d incorrect values removed", count)

        # 4. save data
        self.dataset.to_csv(FILE_CLEANED_DATA, index=False, encoding='utf-8')
        log.info("Cleaned data is in %s", FILE_CLEANED_DATA)

    def create_before_after_labels(self, name_dep, name_arr):
        """
        Creates the lists 'before', 'after' and 'labels' from data.

        Args:
            name_dep: a string containing the name of the departure column, e.g. old_building_type, old_building_usage...
            name_arr: a string containing the name of the arrival column, e.g. new_building_type, new_building_usage...
        """
        all_repartition = {}
        self.before = {}
        self.after = {}

        for status, value in self.dataset[name_dep].value_counts().items():
            if name_dep == OLD_PREFIX+"geographical_position":  # if geo, get only the geo position (South, East, ..) and not the city
                status = status.split(" ")[0]
                if status in self.before:
                    self.before[status] += value
                else:
                    self.before[status] = value
            else:
                self.before[status] = value  # self.dataset[values_before].value_counts()[status]

        for status, value in self.dataset[name_arr].value_counts().items():
            if name_arr == NEW_PREFIX+"geographical_position":  # if geo, get only the geo position (South, East, ..) and not the city
                status = status.split(" ")[0]
                if status in self.after:
                    self.after[status] += value
                else:
                    self.after[status] = value
            else:
                self.after[status] = value  # self.dataset[values_after].value_counts()[status]

        # 2. merge before and after data in the same dictionary
        for status in self.before:
            all_repartition[status] = [self.before[status], 0]
        for status in self.after:
            if status not in all_repartition:
                all_repartition[status] = [0, self.after[status]]
            else:
                all_repartition[status][1] = self.after[status]

        # 3. convert dictionary in 3 arrays
        self.before = []
        self.after = []
        self.labels = []
        for key in all_repartition:
            if not isinstance(key, float):  # to remove nan values
                self.before.append(all_repartition[key][0])
                self.after.append(all_repartition[key][1])
                self.labels.append(key)

    def create_bar_chart(self, name, title):
        """
        Plot before/after charts.

        Args:
            name: a string containing the name of the EV to plot, e.g. building_type, building_usage, landscape, ...
            title: a string containing the title of the plot
        """
        x = np.arange(len(self.labels))  # the label locations
        width = 0.35

        fig, ax = plt.subplots()

        ax.bar(x - width / 2, [154 for _ in range(len(self.labels))], width=width, color="#DCDCDC")  # grey bar
        bef = ax.bar(x - width / 2, self.before, width=width, label='Avant')  # before data
        ax.bar(x + width / 2, [154 for _ in range(len(self.labels))], width=width, color="#DCDCDC")  # grey bar
        aft = ax.bar(x + width / 2, self.after, width=width, label='Après')  # after data

        ax.set_ylabel('Nombre de personnes')
        plt.xticks(x, self.labels, rotation='vertical')
        auto_label(bef, ax)
        auto_label(aft, ax)
        plt.tight_layout()
        ax.legend()
        filename = os.path.join(FOLDER_DISTRIBUTION, "distribution_" + name + ".png")
        fig.savefig(filename)
        ax.set_title(title)
        plt.show()

    def to_chart(self, env, name, title):
        """
        Create before/after data and plot it.
        :param env:
        :param name:
        :param title:
        Args:
            env: a string containing the EV to plot, e.g. building_type, building_usage, landscape...
            name: a string containing the name to save the file
            title: a string containing the title of the plot
        """
        self.create_before_after_labels(self.columns[env]["name_dep"], self.columns[env]["name_arr"])
        self.create_bar_chart(name, title)
