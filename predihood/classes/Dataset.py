# eternal hack for python imports (here to access parent directory)
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..'))

import logging
import pandas as pd
import numpy as np

import predihood.config
from predihood.config import TRAIN_SIZE, TEST_SIZE, RANDOM_STATE, FILE_ENV
from predihood.utility_functions import check_train_test_percentages
from sklearn.ensemble import IsolationForest
from sklearn.model_selection import train_test_split

log = logging.getLogger(__name__)


class Dataset:
    """
    This class represents assessed neighbourhood with their indicators and EV values. There are options, such as removing outliers or underrepresented neighbourhood.
    """
    def __init__(self, data, env, _type, selected_indicators=None, indicators_to_remove=None, train_size=TRAIN_SIZE, test_size=TEST_SIZE, outliers=False, remove_underrepresented=False):
        """
        Constructor of the Dataset class. Initialize attributes.

        Args:
            data: an instance of Data class. Don"t forget to initialize data after created it with "init_all_in_one()" method
            env: a string representing the EV, i.e. a value in ["variable1", "variable2", ..., "variableN"]
            selected_indicators: a list containing the indicators to keep in the dataset
            indicators_to_remove: a list containing the indicators to remove in the dataset
            train_size: a integer or a float corresponding to the size of the dataset used for training
            test_size: a integer or a float corresponding to the size of the dataset used for test
            outliers: True or False to remove outliers from dataset (detected with IsolationForest algorithm)
            remove_underrepresented: a string to represent the variable of the dataset on which the lowest represented value will be removed.
        """
        self.data = data.data[:]  # data must be a Data object
        self.indicators = data.indicators[:]
        self.filtering = data.filtering
        self.normalization = data.normalization
        self.selected_indicators = selected_indicators[:] if selected_indicators is not None else None
        self.indicators_to_remove = indicators_to_remove[:] if indicators_to_remove is not None else None
        self.type = _type
        self.X = None
        self.Y = None
        self.X_train = None
        self.Y_train = None
        self.X_test = None
        self.Y_test = None
        if env in predihood.config.ENVIRONMENT_VARIABLES:
            self.env = env
        else:
            self.env = "building_type"
        self.train_size, self.test_size, _ = check_train_test_percentages(train_size, test_size)
        self.outliers = outliers
        self.remove_underrepresented = remove_underrepresented
        if self.remove_underrepresented not in self.data or self.remove_underrepresented == "None": self.remove_underrepresented = None

    def init_all_in_one(self):
        """
        Initialize the dataset by initializing X and Y ; generating X_train, Y_train, X_test, Y_test ; removing outliers if needed.
        When the type is "unsupervised", split data into X and Y is not relevant (as there is no train/test sets).
        """
        if self.type == "supervised":
            if self.remove_underrepresented is not None: self.remove_underrepresented_neighbourhoods()
            self.init_X()
            self.init_Y()
            self.train_test()
            if self.outliers:
                self.remove_outliers()  # after train_test() because Isolation Forest needs X_train and X_test
                self.init_X()
                self.init_Y()
                self.train_test()  # need to compute train_test to update X and Y after dropping outliers

    def init_X(self):
        """
        Initialize self.X by getting indicators in dataset.
        """
        assert self.data is not None
        if self.selected_indicators is not None:  # select given indicators
            self.X = self.data.loc[:, self.selected_indicators]
            if self.indicators_to_remove:  # remove given indicators
                for indicator in self.indicators_to_remove:
                    if indicator in self.selected_indicators and indicator in self.X.columns:
                        self.selected_indicators.remove(indicator)
                        self.X = self.X.drop([indicator], axis=1)
        else:
            self.X = self.data.loc[:, self.indicators]
            if self.indicators_to_remove:  # remove given indicators
                for indicator in self.indicators_to_remove:
                    if indicator in self.indicators and indicator in self.X.columns:
                        self.indicators.remove(indicator)
                        self.X = self.X.drop([indicator], axis=1)

    def init_Y(self):
        """
        Initialize self.Y by getting EV in dataset.
        """
        assert self.data is not None
        self.Y = self.data[self.env].values

    def train_test(self):
        """
        Create X_train, Y_train, X_test, Y_test with train_test_split method
        """
        if len(self.X) <= 0:
            self.init_X()
        if len(self.Y) <= 0:
            self.init_Y()
        self.X_train, self.X_test, self.Y_train, self.Y_test = train_test_split(self.X, self.Y, train_size=self.train_size, test_size=self.test_size,  random_state=RANDOM_STATE)  # , stratify=self.Y)

    def remove_outliers(self):
        """
        Detect and remove neighbourhood that are outliers from dataset.
        """
        isolation_forest = IsolationForest(random_state=RANDOM_STATE, n_estimators=100)  # IsolationForest is used to detect outliers
        isolation_forest.fit(self.X_train)
        predictions = isolation_forest.predict(self.X_test)
        for i in range(len(predictions)):
            if predictions[i] == -1:  # this neighbourhood is an outlier according to isolation forest algorithm
                # delete neighbourhood that are detected as outliers, ignore if the neighbourhood is not present in the dataset (e.g. because it has been remove as a underrepresented neighbourhood)
                self.data = self.data.drop(i, axis=0, errors="ignore")

    def remove_underrepresented_neighbourhoods(self):
        """
        Remove from dataset neighbourhood that are assessed as underrepresented in the chosen variable (because they bias the prediction).
        """
        values, counts = np.unique(self.data[self.remove_underrepresented], return_counts=True)
        index_of_less_frequent = np.argmin(counts)
        value_of_less_frequent = str(values[index_of_less_frequent])
        self.data = self.data[self.data[self.remove_underrepresented] != value_of_less_frequent]

    def get_environment_variable(self):
        """
        Get values for a given EV for each assessed neighbourhood and store it in a CSV file.
        """
        assert self.env in predihood.config.ENVIRONMENT_VARIABLES
        data_env = pd.DataFrame(self.data[['CODE', self.env]])
        data_env.to_csv(FILE_ENV)

    def get_all_environmental_variable(self):
        """
        Get EV for each assessed neighbourhood and store it in a CSV file.
        """
        columns = ['CODE']
        columns.extend(predihood.config.ENVIRONMENT_VARIABLES)
        data_env = pd.DataFrame(self.data[columns])
        data_env.to_csv(FILE_ENV)
