# eternal hack for python imports (here to access parent directory)
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..'))

import logging
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from area import area
from sklearn.utils._testing import ignore_warnings

from predihood import model
from predihood.classes.Method import Method
from sklearn.metrics import auc, roc_curve
from sklearn.model_selection import cross_val_score, GridSearchCV
from sklearn.tree import DecisionTreeClassifier

log = logging.getLogger(__name__)


class MethodPrediction(Method):
    """
    This class represents a method for predicting EV and compute performance at a national level.
    """
    def __init__(self, name, dataset, classifier):
        """
        Constructor of the MethodPrediction class. Initialize attributes.
        """
        Method.__init__(self, name, dataset, classifier)
        self.prediction = None
        self.accuracy = 0  # in percentage
        self.precision = 0
        self.recall = 0
        self.confusion_matrix = [0, 0, 0, 0]

    @ignore_warnings(category=UserWarning)
    def compute_performance(self):
        """
        Compute performance metrics, i.e. accuracy.
        """
        # cv=len(np.unique(self.dataset.Y)),
        scores = cross_val_score(self.classifier, self.dataset.X, self.dataset.Y, scoring="accuracy", cv=len(np.unique(self.dataset.Y)))
        self.accuracy = scores.mean() * 100

    def predict(self, neighbourhood_code=None):
        """
        Predict one EV for the given neighbourhood. The EV to predict is stored in the dataset as "env" variable.
        """
        neighbourhood_object = model.get_neighbourhood_from_code(neighbourhood_code)
        if neighbourhood_object is None: raise Exception("The IRIS " + neighbourhood_code + " does not exist in the database!")
        neighbourhood_indicators_values = []
        neighbourhood_indicators_names = []
        if "P14_POP" in neighbourhood_object["properties"]["raw_indicators"] and (self.dataset.normalization == "density" or self.dataset.normalization == "population"):
            neighbourhood_area = area(model.get_coords_from_code(neighbourhood_code)) / 1000000
            neighbourhood_population = neighbourhood_object["properties"]["raw_indicators"]["P14_POP"]
            if self.dataset.normalization == "density":
                density = neighbourhood_population/neighbourhood_area
                for indicator in self.dataset.selected_indicators:
                    if indicator in neighbourhood_object["properties"]["raw_indicators"] and density > 0:
                        current_value = neighbourhood_object["properties"]["raw_indicators"][indicator]
                        if current_value == '': current_value = 0.0
                        neighbourhood_indicators_values.append(float(current_value) / density)
                    else:
                        neighbourhood_indicators_values.append(0)
                    neighbourhood_indicators_names.append(indicator)
            elif self.dataset.normalization == "population":
                for indicator in self.dataset.selected_indicators:
                    if indicator in neighbourhood_object["properties"]["raw_indicators"] and neighbourhood_population > 0:
                        current_value = neighbourhood_object["properties"]["raw_indicators"][indicator]
                        if current_value == '': current_value = 0.0
                        neighbourhood_indicators_values.append(float(current_value) / neighbourhood_population)
                    else:
                        neighbourhood_indicators_values.append(0)
                    neighbourhood_indicators_names.append(indicator)
        else:
            for indicator in self.dataset.selected_indicators:
                if indicator in neighbourhood_object["properties"]["raw_indicators"]:
                    current_value = neighbourhood_object["properties"]["raw_indicators"][indicator]
                    if current_value == '': current_value = 0.0
                    neighbourhood_indicators_values.append(float(current_value))
                else:
                    neighbourhood_indicators_values.append(0)
                neighbourhood_indicators_names.append(indicator)

        df = pd.DataFrame([neighbourhood_indicators_values], columns=neighbourhood_indicators_names)
        self.prediction = self.classifier.predict(df)[0]
        log.debug("%s", self.prediction)
