# eternal hack for python imports (here to access parent directory)
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..'))

from predihood.config import RANDOM_STATE

class Method:
    """
    This class represents the general concept of a Method that is applied on data. There are two specific concepts of Method: MethodSelection and MethodPrediction.
    """
    def __init__(self, name, dataset=None, classifier=None):
        """
        Constructor of the Method class. Initialize attributes.
        Args:
            name: a string that represents the name of the method, e.g. "feature selection" or "correlation matrix"
            dataset: a Dataset object on which the method will be applied
            classifier: an object that can be used (e.g. fit) on data
        """
        self.name = name
        self.dataset = dataset
        self.classifier = classifier
        self.parameters = None  # same as return

    def fit(self):
        """
        Fit the classifier on dataset.
        """
        self.classifier.random_state = RANDOM_STATE  # defined random_state to have reproducibility
        if self.dataset.type == "supervised":
            self.classifier.fit(X=self.dataset.X_train, y=self.dataset.Y_train)
        else:
            # unsupervised learning does not need split between X and Y
            if self.dataset.selected_indicators is not None:
                self.classifier.fit(self.dataset.data[self.dataset.selected_indicators])
            else:
                self.classifier.fit(self.dataset.data[self.dataset.indicators])
