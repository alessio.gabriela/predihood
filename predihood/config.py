"""
Configuration variables (e.g., directories, translation).
"""

import os
import json

# 1. define files used by Predihood (reading and writing files)
ROOT_FOLDER = os.path.join("..", "predihood")
FOLDER_DATA = os.path.join(ROOT_FOLDER, "generated_files")
FOLDER_CLASSES = os.path.join(ROOT_FOLDER, "classes")

if not os.path.exists(FOLDER_DATA): os.mkdir(FOLDER_DATA)
FOLDER_DATASETS = os.path.join(ROOT_FOLDER, "datasets")
FOLDER_HIL = os.path.join(FOLDER_DATASETS, "hil")
FOLDER_BIRDS = os.path.join(FOLDER_DATASETS, "bird-migration")
if not os.path.exists(FOLDER_DATASETS): os.mkdir(FOLDER_DATASETS)
FOLDER_DISTRIBUTION = os.path.join(FOLDER_DATA, "distribution-plots")
if not os.path.exists(FOLDER_DISTRIBUTION): os.mkdir(FOLDER_DISTRIBUTION)
FOLDER_SELECTED_INDICATORS = os.path.join(FOLDER_DATA, "selected-indicators")
if not os.path.exists(FOLDER_SELECTED_INDICATORS): os.mkdir(FOLDER_SELECTED_INDICATORS)
FOLDER_ASSESSMENT = os.path.join(FOLDER_DATA, "manual-assessment")
if not os.path.exists(FOLDER_ASSESSMENT): os.mkdir(FOLDER_ASSESSMENT)

FILE_HIERARCHY = os.path.join(FOLDER_CLASSES, "hierarchy.csv")
FILE_DATA_HIL = None  # for legacy reasons, we are not able to provide company's data
FILE_GROUPING = os.path.join(FOLDER_CLASSES, "regrouping.csv")
FILE_CLEANED_DATA = os.path.join(FOLDER_DATA, "cleaned_data.csv")
FILE_ENV = os.path.join(FOLDER_DATASETS, "data.csv")
FILE_SIMILARITIES = os.path.join(FOLDER_DATA, "similarities.csv")
FILE_LIST_DISTRIBUTION = os.path.join(FOLDER_SELECTED_INDICATORS, "selection-distribution.csv")
FILE_MANUAL_ASSESSMENT = os.path.join(FOLDER_ASSESSMENT, "manual_assessment.csv")
FILE_MANUAL_ASSESSMENT_EXPERTS = os.path.join(FOLDER_CLASSES, "iris-expertises-FabienFranckNelly.csv")

# 2. define some constants
TITLES = False  # set True to display titles on plots
OLD_PREFIX, NEW_PREFIX = "old_", "new_"
RANDOM_STATE = 0  # make classifiers deterministic
TRAIN_SIZE, TEST_SIZE = 0.8, 0.2
TOPS_K = [10, 20, 30, 40, 50, 75, 100]  # define top-k to generate lists of selected indicators
PREFERRED_LANGUAGE = "en"

# 4. define EV, the possible values for each one and their translation (because data come from a French company).
# here we create dictionaries that contain translations from english to other languages (specified in config files in json)
CURRENT_CONFIG = ""
CURRENT_CONFIG_DICT = {}
TARGET_VALUES = {}
ENVIRONMENT_VARIABLES = {}
ENVIRONMENT_VALUES = {}
TRANSLATION = {}
NORMALIZATION = ""
VARIABLE_REMOVE_LOW_REPRESENTATIVITY = ""

translations = {
    "en": {
        "KWsearch": "Search",
        "KWalgorithm": "Algorithm",
        "KWresultats": "Results",
        "KWcodeIRIS": "IRIS code",
        "KWnameIRIS": "IRIS name",
        "KWtypeIRIS": "IRIS type",
        "KWpostcodeIRIS": "postcode",
        "KWcitynameIRIS": "city name",
        "KWdepIRIS": "department",
        "startingInfoCarto": "To interact with neighbourhoods, search them by their code or by searching an area (e.g. Lyon) and click on it to display their information. "
                             "<ol><li>If you want to predict the environment of the clicked neighbourhood, just select an algorithm in the list in the popup after clicking on a neighbourhood.</li>"
                             "<li>Otherwise if you want to predict the environment for many neighbourhoods, you can right-click on neighbourhood, select an algorithm in the panel on the left and click on the button to predict.</ul></ol>"
                             "Finally, you can download the result by clicking on the blue download icon.",
        "startingInfoAlgo": "This interface helps you to <b>tune algorithms and to test them</b> on our datasets. To use it:"
                            "<ol>"
                            "<li>Choose an algorithm in the list. You can add your own classifiers by adding them in the folder predihood/algorithms.</li>"
                            "<li>Tune the algorithm as desired. <i>Specific parameters</i> are specific to the algorithm itself and <i>general parameters</i> are common to Scikit-Learn algorithms.</li>"
                            "<li>Tune the training by setting the size of the train/test datasets, the removing of outliers and/or underrepresented neighbourhoods.</li>"
                            "<li>You can now run the algorithm on the dataset with your own configuration by clicking on the button \"Train, test and evaluate\".</li>"
                            "</ol>"
                            "When it is done, you can:"
                            "<ol>"
                            "<li>Compare many results with old ones by clicking on the checkox near the name of the algorithm."
                            "<li>Download the result by clicking on the blue download icon."
                            "</ol>",
        "predihoodInfo": "This project, bringing together social science and computer science researchers and the start-up Home in Love, proposes a website for facilitating real estate searches. "
                         "There are mainly two objectives. The first one is to improve spatial recommending procedures by facilitating the search and the comparison of neighbourhoods in France. "
                         "The second one is to provide a generic interface for the tuning of algorithms. "
                         "The main perspective is to recommend neighbourhoods based on user profiles, integrate more data sources in an automatic way and justify recommendations to users.",
        "infoIRIS": "General information about neighbourhood",
        "propertyCode": "Property code",
        "propertyLabel": "Property label",
        "value": "Value",
        "groupedIRIS": "Grouped indicators for neighbourhood",
        "rawIRIS": "Raw indicators for neighbourhood",
        "indicatorCode": "Indicator code",
        "indicatorLabel": "Indicator label",
        "#inputZoomLevel": "Minimal zoom level to display neighbourhood automatically",
        "#activateLock": "Enable/Disable",
        "actualZoom": "(actual zoom level= ",
        "#boutonRechercherCode": "Search by neighbourhood code",
        "#boutonRechercherNom": "Search by neighbourhood name or city",
        "#boutonEffacer": "Clear",
        "selectAlgo": "Choose an algorithm among the list below.",
        "selectAlgo2": "Select an algorithm",
        "chooseAlgo": " -- select an algorithm --",
        "paramAlgo": "Tune the selected algorithm.",
        "paramAlgo2": "Tune the algorithm",
        "#specificParametersTitle": "Specific parameters",
        "#commonParametersTitle": "General parameters",
        "tuneDataset": "Tune the repartition of the data into train and test sets.",
        "tuneDataset2": "Tune dataset",
        "sizeTraining": "Size for the train set. The value is a percentage and should be between 1 and 99%.",
        "sizeTraining2": "The percentage of the dataset used to train the algorithm.",
        "sizeTraining3": "Train size",
        "sizeTest": "Size for the test set. The value is a percentage and should be between 1 and 99%.",
        "sizeTest2": "The percentage of the dataset used to test the algorithm.",
        "sizeTest3": "Test size",
        "removeOutliers": "Check it to remove outliers in the dataset.",
        "removeOutliers2": "Check it to remove outliers.",
        "removeOutliers3": "Remove outliers",
        "removeRural": "Check it to remove from the dataset under-represented neighbourhoods according to the selected variable.",
        "removeRural2": "Check it to remove under-represented neighbourhoods.",
        "removeRural3": "Remove under-represented neighbourhoods",
        "btnRun": "Train, test and evaluate",
        "btnRun2": "Train and test the tuned algorithm on data.",
        "#clearResults": "Results of the tuned algorithm",
        "#clearResults2": "Clear all results",
        "footer": "Data sources (statistics, neighbourhood)",
        "#btnPredictSelectedIris": "Predict selected neighbourhoods",
        "#downloadManyIris": "Export this table as an Excel file.",
        "indexGoal": "I'm looking for a new living place",
        "indexGoal2": "Search neighbourhoods",
        "indexGoal3": "I want to tune and run classifiers",
        "indexGoal4": "Run classifiers",
        "predictIrisSection1": "Search for one/many neighbourhoods",
        "predictIrisSection2": "Predict for many neighbourhoods"
    },
    "fr": {
        "KWsearch": "Rechercher",
        "KWalgorithm": "Algorithme",
        "KWresultats": "Resultats",
        "KWcodeIRIS": "code quartier",
        "KWnameIRIS": "nom du quartier",
        "KWtypeIRIS": "type du quartier",
        "KWpostcodeIRIS": "code postal",
        "KWcitynameIRIS": "ville",
        "KWdepIRIS": "département",
        "startingInfoCarto": "Pour interagir avec les quartiers, recherchez-les avec leur code ou une zone (e.g. Lyon) et cliquez dessus pour afficher leurs informations. "
                             "<ol><li>Si vous voulez prédire l'environement du quartier cliqué, sélectionnez un algorithme dans la liste dans la popup après avoir cliqué sur un quartier.</li>"
                             "<li>Sinon si vous voulez prédire l'environnement de plusieurs quartiers, faites un clic droit sur les quartiers concernés, sélectionnez un algorithme dans le panel de gauche et cliquez sur le bouton pour prédire.</li>"
                             "Enfin, vous pouvez télécharger le résultat en cliquant sur l'icône bleue de téléchargement.",
        "startingInfoAlgo": "Cette interface vous aide dans <b>la configuration et le test d'algorithmes</b> sur nos jeux de données. Pour l'utiliser :"
                            "<ol>"
                            "<li>Choisissez un algorithme dans la liste. Vous pouvez ajouter vos propres algorithmes en les ajoutant dans le dossier predihood/algorithms.</li>"
                            "<li>Paramétrez l'algorithme à votre souhait. Les <i>paramètres spécifiques</i> sont spécifiques à l'algorithme lui-même) et les <i>paramètres généraux</i> sont communs aux algorithmes Scikit-Learn. "
                            "<li>Paramétrez l'entraînement en changeant la taille des données d'entraînement et de test, le retrait des quartiers \"outliers\" et/ou des quartiers ruraux.</li>"
                            "<li>Vous pouvez maintenant lancer l'algorithme (avec votre propre configuration) sur le jeu de données en cliquant sur le bouton \"Entraîner, tester et évaluer\".</li>"
                            "</ol>"
                            "Quand cela est fini, vous pouvez :"
                            "<ol>"
                            "<li>Comparer plusieurs résultats avec d'anciens résultats en cochant la case correspondante."
                            "<li>Télécharger les réusltats en cliquant sur l'icône bleue. "
                            "</ol>",
        "predihoodInfo": "Ce projet, réunissant des chercheurs en sciences sociales et en informatique et la start-up Home in Love, propose un site internet pour faciliter les recherches immobilières. "
                         "Il y a principalement deux objectifs. Le premier est d'améliorer les procédures de recommandation spatiale en facilitant la recherche et la comparaison des quartiers en France. "
                         "Le second est de fournir une interface générique pour le paramétrage des algorithmes. "
                         "La perspective principale est de recommander des quartiers en fonction des profils d'utilisateurs, d'intégrer davantage de sources de données de manière automatique et de justifier les recommandations aux utilisateurs.",
        "infoIRIS": "Informations générales à propos du quartier",
        "propertyCode": "Code de la propriété",
        "propertyLabel": "Label de la propriété",
        "value": "Valeur",
        "groupedIRIS": "Indicateurs regroupés pour le quartier",
        "rawIRIS": "Indicateurs bruts pour le quartier",
        "indicatorCode": "Code de l'indicateur",
        "indicatorLabel": "Label de l'indicateur",
        "#inputZoomLevel": "Niveau de zoom minimal pour afficher les quartiers automatiquement",
        "#activateLock": "Activer/Désactiver",
        "actualZoom": "(niveau de zoom actuel = ",
        "#boutonRechercherCode": "Rechercher par code quartier",
        "#boutonRechercherNom": "Rechercher par nom ou par ville",
        "#boutonEffacer": "Effacer",
        "selectAlgo": "Choisir un algorithme parmi la liste ci-dessous.",
        "selectAlgo2": "Choisir un algorithme",
        "chooseAlgo": "-- choisir un algorithme --",
        "paramAlgo": "Paramétrer l'algorithme choisi.",
        "paramAlgo2": "Paramétrer l'algorithme",
        "#specificParametersTitle": "Paramètres spécifiques",
        "#commonParametersTitle": "Paramètres généraux",
        "tuneDataset": "Paramétrer la répartition entre les jeux d'apprentissage et de test.",
        "tuneDataset2": "Jeux de données",
        "sizeTraining": "Taille des données d'entraînement. La valeur est un pourcentage entre 1 et 99%.",
        "sizeTraining2": "Le pourcentage des données utilisées pour l'apprentissage.",
        "sizeTraining3": "Données d'entraînement",
        "sizeTest": "Taille des données de test. La valeur est un pourentage entre 1 et 99%.",
        "sizeTest2": "Le pourcentage des données utilisées pour le test.",
        "sizeTest3": "Données de test",
        "removeOutliers": "Cocher pour retirer les IRIS détectés comme abberants du jeu de données.",
        "removeOutliers2": "Cocher pour retirer les IRIS abberants.",
        "removeOutliers3": "Retirer les IRIS abberants",
        "removeRural": "Cocher pour retirer du jeu de données les IRIS les moins représentés selon la variable sélectionnée.",
        "removeRural2": "Cocher pour retirer les IRIS les moins rerésentés.",
        "removeRural3": "Retirer les IRIS les moins représentés",
        "btnRun": "Entraîner, tester et évaluer",
        "btnRun2": "Entraîner et tester l'algorithme paramétré sur les données.",
        "#clearResults": "Résultats de l'algorihtme paramétré",
        "#clearResults2": "Supprimer tous les résultats",
        "footer": "Sources de données (statistiques, quartiers/IRIS)",
        "#btnPredictSelectedIris": "Prédire les quartiers sélectionnés",
        "#downloadManyIris": "Exporter cette table comme un fichier Excel.",
        "indexGoal": "Je suis à la recherche d'un nouveau lieu de vie",
        "indexGoal2": "Rechercher un quartier",
        "indexGoal3": "Je souhaite paramétrer mes algorithmes",
        "indexGoal4": "Paramétrer mes algorithmes",
        "predictIrisSection1": "Rechercher un/des quartiers",
        "predictIrisSection2": "Prédire pour plusieurs quartiers"
    }
}
