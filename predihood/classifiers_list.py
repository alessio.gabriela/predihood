"""
Produces the list of available classifiers: 6 from the [Scikit-learn library](https://scikit-learn.org/), and Python files added in `algorithms/`.
"""

# eternal hack for python imports (here to access parent directory)
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

# define available classifiers for the algorithmic interface
import re

from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier

import importlib
tree = os.listdir('algorithms')
regex = re.compile(r'__[^_]*__')
tree_filtered = [i for i in tree if not regex.match(i) and i.endswith('.py')]  # remove not .py files (eg, __pycache__)

AVAILABLE_CLASSIFIERS = {
    "Random Forest Classifier": RandomForestClassifier,
    "KNeighbors Classifier": KNeighborsClassifier,
    "Decision Tree Classifier": DecisionTreeClassifier,
    "SVC": SVC,
    "AdaBoost Classifier": AdaBoostClassifier,
    "MLP Classifier": MLPClassifier
}

for i in tree_filtered:
    name = i[:-3]  # remove .py extension to get only the name of the class
    module = importlib.import_module("predihood.algorithms."+name)
    class_ = getattr(module, name)  # create an object representing the algorithm
    AVAILABLE_CLASSIFIERS[name] = class_
